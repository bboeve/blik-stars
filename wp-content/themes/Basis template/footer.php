<footer id="footer" class="site-footer">
	<div class="container">
		<div class="row">
			<div class="site-footer-inner sidebar">
				<div class="col-md-3 col-sm-6">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer1'); ?>
				</div>
				<div class="col-md-3 col-sm-6">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer2'); ?>
				</div>
				<div class="col-md-3 col-sm-6">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer3'); ?>
				</div>
				<div class="col-md-3 col-sm-6">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('footer4'); ?>
				</div>
			</div>

			<div class="colophon">
				<div class="site-info col-md-6">
					<p><?php _e('Copyright','rby'); ?> &copy; <?php echo date('Y'); ?> - <?php bloginfo('name'); ?> </p>
				</div>

				<nav id="footernav" class="footernav col-md-6">
					<?php wp_nav_menu( array(
						'container' => '',
						'fallback_cb' => 'false',
						'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'theme_location' => 'footernav', 
						'depth' => '1'
					));?>
				</nav>
			</div>
		</div><!-- close .row -->
	</div><!-- close .container -->
</footer><!-- close #colophon -->


<?php wp_footer(); ?>

	<!--[if IE 6]>
		<div class="ie6">
			<p><?php _e('You are using a very old version of Internet Explorer. For the best experience please upgrade (for free) to a modern browser:','rby'); ?>
			<ul>
				<li><a href="http://www.mozilla.com/" rel="nofollow external">Firefox</a></li>
				<li><a href="http://www.google.com/chrome/" rel="nofollow external">Google Chrome</a></li>
				<li><a href="http://www.apple.com/safari/" rel="nofollow external">Safari</a></li>
				<li><a href="http://www.microsoft.com/windows/internet-explorer/" rel="nofollow external">Internet Explorer</a></li>
			</ul>
		</div>
	<![endif]-->
</body>
</html>