<?php get_header(); ?>
<div class="frontpage">
	<div class="container">
		<div class="row">
			<div id="subheader" class="main-content-inner col-md-12 text-center">
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<h2 class="description"><?php bloginfo( 'description' ); ?></h2>
			</div>
		</div>
	</div>
</div>
<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-10 col-md-offset-1">

				<?php while ( have_posts() ) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h2><?php the_title(); ?></h2>
					
						<div class="entry-content">
							<?php the_content(); ?>
							<?php
								wp_link_pages( array(
									'before' => '<div class="page-links">' . __( 'Pages:', 'rby' ),
									'after'  => '</div>',
								) );
							?>

						</div><!-- .entry-content -->
					</article><!-- #post-## -->

				<?php endwhile; // end of the loop. ?>
				
			</div><!-- close .main-content-inner -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<?php get_footer(); ?>
