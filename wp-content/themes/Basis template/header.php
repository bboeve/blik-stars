<!DOCTYPE html>
<!--[if IE 7 ]><html class="no-js ie ie7" lang="nl"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="nl"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9" lang="nl"><![endif]-->
<!--[if gt IE 9]><html class="no-js ie" lang="nl"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php do_action( 'before' ); ?>
<?php if(get_theme_mod( 'rby_cookie_checkbox' ) == 'true') { ?>
<!--googleoff: all-->
<div role="dialog" aria-live="polite" id="cookie-notice">
	<span class="cookie-notice-text">
		<?php echo get_theme_mod( 'rby_cookie_text' ); ?>
		<a aria-label="more info" role="button" tabindex="0" class="cookie-link" href="<?php echo get_theme_mod( 'rby_policy_url' ); ?>" target="_blank"><?php echo get_theme_mod( 'rby_policy_text' ); ?></a>
	</span>
	<a aria-label="dismiss cookie message" role="button" tabindex="0" class="cookie-dismiss"><?php echo get_theme_mod( 'rby_cookie_btn' ); ?></a></div>
</div>
<!--googleon: all-->
<?php } ?>


<header id="masthead" class="site-header">
	<div class="container">
		<div class="row">
			<div class="site-header-inner">
				<div class="site-branding col-md-6">
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<p class="site-description"><?php bloginfo( 'description' ); ?></p>
				</div>

				<nav id="topnav" class="topnav col-md-6">				
					<?php wp_nav_menu( array(
							'theme_location' => 'topnav',
							'fallback_cb' => 'false',
							'container' => 'topmenu',
							'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth' => '1'
						)); 
					?>
				</nav>
			</div>
		</div>
	</div><!-- .container -->
</header><!-- #masthead -->

<nav class="site-navigation">
	<div class="container">
		<div class="row">
			<div class="site-navigation-inner col-md-12">
				<div class="navbar navbar-default">
				<div class="handle">
					Menu
					<div class="mobile-icon">
						<div class="line"></div>
						<div class="line"></div>
						<div class="line"></div>
					</div>
				</div>
				<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'menu_class' => 'main-nav',
							'fallback_cb' => 'true',
							'menu_id' => 'main-nav',
							'depth' => '0'
						)
					); ?>
				</div><!-- .navbar -->
			</div>
		</div>
	</div><!-- .container -->
</nav><!-- .site-navigation -->


