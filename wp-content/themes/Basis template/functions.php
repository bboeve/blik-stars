<?php

/**
 * Make theme available for translation
 * use rby to trigger the translation
*/
load_theme_textdomain( 'rby', get_template_directory() . '/assets/lang' );


/**
 *	Move wp-core jQuery to footer
 */
function rby_jquery_into_footer() {

    if( is_admin() ) {
        return;
    }

    $wp_scripts = wp_scripts();

    $wp_scripts->add_data( 'jquery',         'group', 1 );
    $wp_scripts->add_data( 'jquery-core',    'group', 1 );
    $wp_scripts->add_data( 'jquery-migrate', 'group', 1 );
}

add_action( 'wp_enqueue_scripts', 'rby_jquery_into_footer', 0 );

/**
 *	Enqueue styles & scripts
 */
function rby_scripts() {
	// load main styling 
		wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css' );
	// load main JS file
		wp_enqueue_script( 'rby-js', get_template_directory_uri() . '/assets/js/all.js' , array('jquery'), null, true);
	}
add_action( 'wp_enqueue_scripts', 'rby_scripts' );

/**
 *  Enqueue Google Fonts
 */
function rby_load_fonts() {
        wp_register_style( 'googleFonts', '//fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i' );
        wp_enqueue_style( 'googleFonts' );
    }
add_action('get_footer', 'rby_load_fonts');


/**
 * Define wp_nav_menu() options.
*/
register_nav_menus( array(
	'primary'  => __( 'Primarymenu', 'rby' ),
) );

register_nav_menus( array(
	'topnav'  => __( 'Headermenu', 'rby' ),
) );

register_nav_menus( array(
	'footernav'  => __( 'Footermenu', 'rby' ),
) );

add_theme_support( 'post-thumbnails', array( 'post' ) );

/**
 * Include php files
 */
include('assets/inc/functions.misc.php');
//include('assets/inc/functions.sidebars.php');
include('assets/inc/functions.customizer.php');
//include('assets/inc/functions.posttypes.php');

/**
 * Include widget php files
 */
 //include('assets/inc/widgets/widget.information.php');
 //include('assets/inc/widgets/widget.social.php');