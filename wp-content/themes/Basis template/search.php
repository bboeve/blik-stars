<?php get_header(); ?>
<div class="main-content">
	<div class="container">
		<div class="row">

			<div id="content" class="main-content-inner col-md-10 col-md-offset-1">		
			<?php if (have_posts()) : ?>
			
				<h1>
					<?php printf(__( 'Search Results for: %1$s','rby'),'<span class="search-highlight">'.get_search_query().'</span>'); ?>
				</h1>
				
				<p>
					<?php 
						$allsearch = new WP_Query('s='.get_search_query().'&showposts=-1');
						$count = $allsearch->post_count;
						wp_reset_query();
						printf(__('Found %2$s articles containing the keyword: %1$s','rby'), '<span class="search-highlight">'.get_search_query().'</span>', $count);
					?>
				</p>
				
			<?php while (have_posts()) : the_post(); ?>
			
				<article <?php post_class(); ?>>
				
					<h2 class="search-title">
						<a href="<?php the_permalink() ?>">
							<?php the_title(); ?>
						</a>
					</h2>
					
					<p>
						<?php the_excerpt(); ?>
						
						<a  class="more" href="<?php echo the_permalink(); ?>">
							<?php _e('Read more','rby'); ?>
						</a>
					</p>
					
				</article>
				
			<?php endwhile; ?>
				
			<?php else : ?>
			
				<h1>
					<?php printf(__( 'Search Results for: %1$s','rby'),'<strong class="search-highlight">'.get_search_query().'</strong>'); ?>
				</h1>
				
				<p>
					<?php printf(__('Your search for <em>&quot;%1$s&quot;</em> did not match any documents. Please make sure all your words are spelled correctly or try different keywords.','rby'),get_search_query() );?>
				</p>
				
				<p>
					<?php get_search_form();?>
				</p>
				
			<?php endif; ?>	   
			
			<?php the_posts_pagination( array(
				'mid_size' => 2,
			    'screen_reader_text' => ' ', 
			    'prev_text'          => '&larr; '.__( 'Previous', 'rby' ).'',
			    'next_text'          => ''.__( 'Next', 'rby' ).' &rarr;'
			) );?>
			
		</div>
	</div><!-- close .main-content-inner -->
			</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<?php get_footer(); ?>