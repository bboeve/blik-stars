<?php get_header(); ?>

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-md-8 col-sm-12">
		
			<h1>
				<?php 
					if( is_category() || is_tag() || is_tax() )
						single_term_title( __( 'Posts about', 'rby' ) . ' ' );
					elseif( is_author() )
						echo __( 'Posts by', 'rby' ) . ' ' . get_the_author_meta( 'display_name' );
					else
						_e( 'News', 'rby' );
				?>
			</h1>
			
			<?php if( have_posts() ) : ?>

				<?php while( have_posts() ) : the_post(); ?>
			
					<article <?php post_class('clearfix'); ?>>
					
						<h2>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
												
						<p class="meta">
							<?php _e( 'Posted on', 'rby' ); ?> <time datetime="<?php the_time( 'Y-m-d' ); ?>"><?php echo get_the_date(); ?></time>
							<?php _e( 'by', 'rby' ) ?> <?php the_author_posts_link(); ?> 
							<?php _e( 'in the category', 'rby' ) ?> <?php the_category( ', ' ) ?>
						</p>
						
						<?php if ( has_post_thumbnail() ) { ?>
							<figure class="thumb">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'thumbnail' ); ?>
								</a>
							</figure>

						<?php } else { ?>
							
						<?php } ?>
						
						<?php the_excerpt(); ?>
						
						<a class="more" href="<?php the_permalink();?>"> <?php _e('Read more','rby');?> </a>
					</article>

		
				<?php endwhile; ?>
		

		
			<?php endif; ?>
		
			<?php the_posts_pagination( array(
				'mid_size' => 2,
			    'screen_reader_text' => ' ', 
			    'prev_text'          => '&larr; '.__( 'Previous', 'rby' ).'',
			    'next_text'          => ''.__( 'Next', 'rby' ).' &rarr;'
			) );?>
			
			</div><!-- close .main-content-inner -->

			<div class="sidebar col-md-4 col-sm-12">

				<?php // add the class "panel" below here to wrap the sidebar in Bootstrap style ;) ?>

				<div class="sidebar-padder">
					<?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('blog'); ?>
				</div>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->

<?php get_footer(); ?>