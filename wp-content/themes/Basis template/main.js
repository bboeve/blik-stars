// @codekit-prepend 'assets/js/jquery.fancybox.js'
// @codekit-prepend 'assets/js/jquery.flexslider.js'
// @codekit-prepend 'assets/js/jquery.cookieBar.js'

jQuery(document).ready(function($){

// Theme Main JQuery functions
	$('.handle').on('click', function() {
		$('nav ul.main-nav').toggleClass('show');
	});

// Adding :hover & :focus states on Mobile
	window.onload = function() {
	  if(/iP(hone|ad)/.test(window.navigator.userAgent)) {
	    document.body.addEventListener('touchstart', function() {}, false);
	  }
	};

// jQuery Cookie notice
	$('#cookie-notice').cookieBar({ 
		closeButton : '.cookie-dismiss', 
		hideOnClose: false });
	$('#cookie-notice').on('cookieBar-close', function() { 
	 	$(this).fadeOut('1s'); 
	});

// Initialize the Lightbox and add rel="gallery" to all gallery images when the gallery is set up using [gallery link="file"] so that a Lightbox Gallery exists
	$("[data-fancybox]").fancybox({
		youtube : {
			showinfo : 0,
		}
	});

	$(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").attr('data-fancybox','gallery').fancybox();
	$('a[href$="jpg"], a[href$="png"], a[href$="jpeg"]').fancybox({
		thumbs     		: false,
		fullScreen 		: false,
		thumbs     		: false,
		loop	   		: false,
		slideShow  		: false,
		clickContent    : false,
	    buttons : [
	        'slideShow',
	        'fullScreen',
	        'thumbs',
	        //'share',
	        //'download',
	        //'zoom',
	        'close'
	    ],
	});


});

