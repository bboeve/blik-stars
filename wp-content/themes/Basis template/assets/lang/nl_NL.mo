��    V      �     |      x     y  {   �  B     "   K  :   n  S   �     �     	     	  	   #	     -	     2	     8	  V   ?	     �	     �	     �	     �	     �	     �	     �	  	   
     
     
     &
     3
     7
     N
     c
     l
     u
     ~
  
   �
  0   �
     �
  
   �
  
   �
     �
     �
               5     B  	   I     S     X  <   ]  G   �      �          
  	              ,     5     >     M     Y  	   r      |     �     �     �     �  (   �     �  0     '   F     n     z     �     �     �  	   �     �     �     �     �     �  }        �  �   �     0     3     C  h  U     �  x   �  E   N  -   �  A   �  L        Q     W     p          �  
   �  
   �  W   �     �                )     >     Q     `  	   o     y     ~     �     �     �     �     �     �     �     �  
   �  7   �     '     B     J     V     d     q     �     �     �     �     �     �  2   �  G        S  	   m     w     �     �     �     �     �  	   �     �  	   �     �                     :  "   L     o  .   �  +   �     �     �               3     D     M     S  
   _  
   j     u  �   �     /  �   ;     �     �  
   �     J   D              ,      9           <         *   (      E   .       O   	                 $           S       I   "            &   5   8       Q   /   L         >               '   0                      T   
   R      ;       )                    !      G       ?   4          N                   V              %   B   K          6       P   @      C   1                 H      M         2           3           :   U   =   7       A   +   -   F                      #    Accept button text Add all the needed company information in one of the sections below to show it around the website or in one of the widgets. Add all your social media links below to show them on your website Add and delete your cookie notice. Add the general contact information of your company below. Add the needed registration & financial numbers below to show them on your website. Address Back to the overview Bank No. Bank name Blog CC No CC No. Change the contents of this widget on the <a href="%1$s">contact information</a> page. City Comments are closed Company name Connect with us on LinkedIn Contact information Cookie Notice Cookie notice text Copyright Country E-mail Facebook URL Fax Follow us on Instagram Follow us on Twitter Footer 1 Footer 2 Footer 3 Footer 4 Footermenu Found %2$s articles containing the keyword: %1$s General information Headermenu Icon types Instagram URL Large icons Like our Facebook page Link to privacy policy LinkedIn URL Mobile More link News Next Nothing could be found at this location. Maybe try a search? One response to &ldquo;%2$s&rdquo; %1$s responses to &ldquo;%2$s&rdquo; Oops! Something went wrong here. Pages: Postal code Posted on Posts about Posts by Previous Primary button Primarymenu Privacy policy link text Read more Registration numbers & financial Reply Search Search Results for: %1$s Secondary button Show RSS feed in the social media widget Show the cookie notice. Shows links to specified social network profiles Shows the specified contact information Small icons Small icons with text Social media links Subscribe to our RSS Subscribe via RSS Telephone Title Twitter URL VAT No VAT No. View our YouTube channel You are using a very old version of Internet Explorer. For the best experience please upgrade (for free) to a modern browser: YouTube URL Your search for <em>&quot;%1$s&quot;</em> did not match any documents. Please make sure all your words are spelled correctly or try different keywords. by in the category labelSearch for: Project-Id-Version: rby v1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-12-04 15:45+0100
PO-Revision-Date: 2018-12-04 15:45+0100
Last-Translator: Ruben Zwiers <ruben@rubsel.com>
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.0.9
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: ..
 Accepteer button tekst Vul bij een van de onderdelen hieronder de nodige bedrijfsinformatie in om deze op de website of in de widgets te tonen. Vul hieronder alle social media links in om ze op de website te tonen Voeg een cookie melding toe of verwijder deze Vul hieronder de algemene contact informatie in van jouw bedrijf. Hieronder kan je alle benodigde registratie- & financiele nummers toevoegen. Adres Terug naar het overzicht Rekeningnummer Banknaam Blog KVK nummer KVK nummer Pas de inhoud van deze widget aan via de <a href="%1$s">contact information</a> pagina. Stad Reageren niet meer mogelijk Bedrijfsnaam Verbind via LinkedIn Contact informatie Cookie melding Cookie melding Copyright Land E-mail Facebook URL Fax Volg ons op Instagram Volg ons op Twitter Footer 1 Footer 2 Footer 3 Footer 4 Footermenu Er zijn %2$s berichten gevonden met het zoekwoord: %1$s Algemene contactinformatie Topmenu Icoon types Instagram URL Grote iconen Like onze Facebook pagina URL naar privacystatement LinkedIn URL Mobiel Lees meer link Nieuws Volgende Er is hier niets gevonden. Probeer eens te zoeken: Één reactie op &ldquo;%2$s&rdquo; %1$s reacties op &ldquo;%2$s&rdquo; Oeps! Hier ging iets mis. Pagina's: Postcode Geplaatst op Berichten over Berichten door Vorige Primaire button Hoofdmenu Privacystatement link tekst Lees meer Registraties & financieel Beantwoorden Zoeken Zoekresultaten voor: %1$s Secundaire button Laat de RSS feed in de widget zien Toon cookie melding. Laat de ingevulde sociale media profielen zien Laat de ingevulde contact informatie zien.  Kleine iconen Kleine iconen met tekst Sociale media links Abonneer op onze RSS-feed Abonneer via RSS Telefoon Titel Twitter URL BTW nummer BTW nummer Bekijk ons YouTube kanaal Je maakt gebruik van een oude versie van Internet Explorer. Voor de beste ervaring op deze website raden we je aan (gratis) een modernere browser te gebruiken. YouTube URL Het zoeken naar <em>&quot;%1$s&quot;</em> heft geen resultaat opgeleverd. Zorg dat je zoekwoorden goed gespeld zijn of probeer het nog eens met andere termen.  door in de categorie Zoek naar: 