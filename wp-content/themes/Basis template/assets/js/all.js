"use strict";
// ==================================================
// fancyBox v3.3.5
//
// Licensed GPLv3 for open source use
// or fancyBox Commercial License for commercial use
//
// http://fancyapps.com/fancybox/
// Copyright 2018 fancyApps
//
// ==================================================
function _typeof(t){return(_typeof="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function t(e){return typeof e}:function t(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e})(t)}
/*
 * jQuery FlexSlider v2.7.0
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */function _defineProperty(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}
// @codekit-prepend 'assets/js/jquery.fancybox.js'
// @codekit-prepend 'assets/js/jquery.flexslider.js'
// @codekit-prepend 'assets/js/jquery.cookieBar.js'
// ==================================================
// fancyBox v3.3.5
//
// Licensed GPLv3 for open source use
// or fancyBox Commercial License for commercial use
//
// http://fancyapps.com/fancybox/
// Copyright 2018 fancyApps
//
// ==================================================
!function(u,c,v,m){// Default click handler for "fancyboxed" links
// ============================================
function n(t,e){var n=[],a=0,o,i;// Avoid opening multiple times
t&&t.isDefaultPrevented()||(t.preventDefault(),// Get all related items and find index for clicked one
(i=(o=(e=t&&t.data?t.data.options:e||{}).$target||v(t.currentTarget)).attr("data-fancybox")||"")?// Sometimes current item can not be found (for example, if some script clones items)
(a=(n=(n=e.selector?v(e.selector):t.data?t.data.items:[]).length?n.filter('[data-fancybox="'+i+'"]'):v('[data-fancybox="'+i+'"]')).index(o))<0&&(a=0):n=[o],v.fancybox.open(n,e,a))}// Create a jQuery plugin
// ======================
// If there's no jQuery, fancyBox can't work
// =========================================
if(u.console=u.console||{info:function t(e){}},v)// Check if fancyBox is already initialized
// ========================================
if(v.fn.fancybox)console.info("fancyBox already initialized");else{// Private default settings
// ========================
var t={
// Enable infinite gallery navigation
loop:!1,
// Horizontal space between slides
gutter:50,
// Enable keyboard navigation
keyboard:!0,
// Should display navigation arrows at the screen edges
arrows:!0,
// Should display counter at the top left corner
infobar:!0,
// Should display close button (using `btnTpl.smallBtn` template) over the content
// Can be true, false, "auto"
// If "auto" - will be automatically enabled for "html", "inline" or "ajax" items
smallBtn:"auto",
// Should display toolbar (buttons at the top)
// Can be true, false, "auto"
// If "auto" - will be automatically hidden if "smallBtn" is enabled
toolbar:"auto",
// What buttons should appear in the top right corner.
// Buttons will be created using templates from `btnTpl` option
// and they will be placed into toolbar (class="fancybox-toolbar"` element)
buttons:["zoom",//"share",
//"slideShow",
//"fullScreen",
//"download",
"thumbs","close"],
// Detect "idle" time in seconds
idleTime:3,
// Disable right-click and use simple image protection for images
protect:!1,
// Shortcut to make content "modal" - disable keyboard navigtion, hide buttons, etc
modal:!1,image:{
// Wait for images to load before displaying
//   true  - wait for image to load and then display;
//   false - display thumbnail and load the full-sized image over top,
//           requires predefined image dimensions (`data-width` and `data-height` attributes)
preload:!1},ajax:{
// Object containing settings for ajax request
settings:{
// This helps to indicate that request comes from the modal
// Feel free to change naming
data:{fancybox:!0}}},iframe:{
// Iframe template
tpl:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
// Preload iframe before displaying it
// This allows to calculate iframe content width and height
// (note: Due to "Same Origin Policy", you can't get cross domain data).
preload:!0,
// Custom CSS styling for iframe wrapping element
// You can use this to set custom iframe dimensions
css:{},
// Iframe tag attributes
attr:{scrolling:"auto"}},
// Default content type if cannot be detected automatically
defaultType:"image",
// Open/close animation type
// Possible values:
//   false            - disable
//   "zoom"           - zoom images from/to thumbnail
//   "fade"
//   "zoom-in-out"
//
animationEffect:"zoom",
// Duration in ms for open/close animation
animationDuration:366,
// Should image change opacity while zooming
// If opacity is "auto", then opacity will be changed if image and thumbnail have different aspect ratios
zoomOpacity:"auto",
// Transition effect between slides
//
// Possible values:
//   false            - disable
//   "fade'
//   "slide'
//   "circular'
//   "tube'
//   "zoom-in-out'
//   "rotate'
//
transitionEffect:"fade",
// Duration in ms for transition animation
transitionDuration:366,
// Custom CSS class for slide element
slideClass:"",
// Custom CSS class for layout
baseClass:"",
// Base template for layout
baseTpl:'<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><div class="fancybox-toolbar">{{buttons}}</div><div class="fancybox-navigation">{{arrows}}</div><div class="fancybox-stage"></div><div class="fancybox-caption"></div></div></div>',
// Loading indicator template
spinnerTpl:'<div class="fancybox-loading"></div>',
// Error message template
errorTpl:'<div class="fancybox-error"><p>{{ERROR}}</p></div>',btnTpl:{download:'<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;"><svg viewBox="0 0 40 40"><path d="M13,16 L20,23 L27,16 M20,7 L20,23 M10,24 L10,28 L30,28 L30,24" /></svg></a>',zoom:'<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}"><svg viewBox="0 0 40 40"><path d="M18,17 m-8,0 a8,8 0 1,0 16,0 a8,8 0 1,0 -16,0 M24,22 L31,29" /></svg></button>',close:'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg viewBox="0 0 40 40"><path d="M10,10 L30,30 M30,10 L10,30" /></svg></button>',
// This small close button will be appended to your html/inline/ajax content by default,
// if "smallBtn" option is not set to false
smallBtn:'<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}"><svg viewBox="0 0 32 32"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg></button>',
// Arrows
arrowLeft:'<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;"><svg viewBox="0 0 40 40"><path d="M18,12 L10,20 L18,28 M10,20 L30,20"></path></svg></a>',arrowRight:'<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;"><svg viewBox="0 0 40 40"><path d="M10,20 L30,20 M22,12 L30,20 L22,28"></path></svg></a>'},
// Container is injected into this element
parentEl:"body",
// Focus handling
// ==============
// Try to focus on the first focusable element after opening
autoFocus:!1,
// Put focus back to active element after closing
backFocus:!0,
// Do not let user to focus on element outside modal content
trapFocus:!0,
// Module specific options
// =======================
fullScreen:{autoStart:!1},
// Set `touch: false` to disable dragging/swiping
touch:{vertical:!0,
// Allow to drag content vertically
momentum:!0},
// Hash value when initializing manually,
// set `false` to disable hash change
hash:null,
// Customize or add new media types
// Example:
/*
        media : {
            youtube : {
                params : {
                    autoplay : 0
                }
            }
        }
        */
media:{},slideShow:{autoStart:!1,speed:4e3},thumbs:{autoStart:!1,
// Display thumbnails on opening
hideOnClose:!0,
// Hide thumbnail grid when closing animation starts
parentEl:".fancybox-container",
// Container is injected into this element
axis:"y"},
// Use mousewheel to navigate gallery
// If 'auto' - enabled for images only
wheel:"auto",
// Callbacks
//==========
// See Documentation/API/Events for more information
// Example:
/*
        afterShow: function( instance, current ) {
            console.info( 'Clicked element:' );
            console.info( current.opts.$orig );
        }
    */
onInit:v.noop,
// When instance has been initialized
beforeLoad:v.noop,
// Before the content of a slide is being loaded
afterLoad:v.noop,
// When the content of a slide is done loading
beforeShow:v.noop,
// Before open animation starts
afterShow:v.noop,
// When content is done loading and animating
beforeClose:v.noop,
// Before the instance attempts to close. Return false to cancel the close.
afterClose:v.noop,
// After instance has been closed
onActivate:v.noop,
// When instance is brought to front
onDeactivate:v.noop,
// When other instance has been activated
// Interaction
// ===========
// Use options below to customize taken action when user clicks or double clicks on the fancyBox area,
// each option can be string or method that returns value.
//
// Possible values:
//   "close"           - close instance
//   "next"            - move to next gallery item
//   "nextOrClose"     - move to next gallery item or close if gallery has only one item
//   "toggleControls"  - show/hide controls
//   "zoom"            - zoom image (if loaded)
//   false             - do nothing
// Clicked on the content
clickContent:function t(e,n){return"image"===e.type&&"zoom"},
// Clicked on the slide
clickSlide:"close",
// Clicked on the background (backdrop) element;
// if you have not changed the layout, then most likely you need to use `clickSlide` option
clickOutside:"close",
// Same as previous two, but for double click
dblclickContent:!1,dblclickSlide:!1,dblclickOutside:!1,
// Custom options when mobile device is detected
// =============================================
mobile:{idleTime:!1,clickContent:function t(e,n){return"image"===e.type&&"toggleControls"},clickSlide:function t(e,n){return"image"===e.type?"toggleControls":"close"},dblclickContent:function t(e,n){return"image"===e.type&&"zoom"},dblclickSlide:function t(e,n){return"image"===e.type&&"zoom"}},
// Internationalization
// ====================
lang:"en",i18n:{en:{CLOSE:"Close",NEXT:"Next",PREV:"Previous",ERROR:"The requested content cannot be loaded. <br/> Please try again later.",PLAY_START:"Start slideshow",PLAY_STOP:"Pause slideshow",FULL_SCREEN:"Full screen",THUMBS:"Thumbnails",DOWNLOAD:"Download",SHARE:"Share",ZOOM:"Zoom"},de:{CLOSE:"Schliessen",NEXT:"Weiter",PREV:"Zurück",ERROR:"Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es später nochmal.",PLAY_START:"Diaschau starten",PLAY_STOP:"Diaschau beenden",FULL_SCREEN:"Vollbild",THUMBS:"Vorschaubilder",DOWNLOAD:"Herunterladen",SHARE:"Teilen",ZOOM:"Maßstab"}}},o=v(u),r=v(c),i=0,s=function t(e){return e&&e.hasOwnProperty&&e instanceof v},f=u.requestAnimationFrame||u.webkitRequestAnimationFrame||u.mozRequestAnimationFrame||u.oRequestAnimationFrame||// if all else fails, use setTimeout
function(t){return u.setTimeout(t,1e3/60)},h=function(){var t=c.createElement("fakeelement"),e,n={transition:"transitionend",OTransition:"oTransitionEnd",MozTransition:"transitionend",WebkitTransition:"webkitTransitionEnd"};for(e in n)if(t.style[e]!==m)return n[e];return"transitionend"}(),g=function t(e){return e&&e.length&&e[0].offsetHeight},l=function t(e,n){var a=v.extend(!0,{},e,n);return v.each(n,function(t,e){v.isArray(e)&&(a[t]=e)}),a},d=function t(e,n,a){var o=this;o.opts=l({index:a},v.fancybox.defaults),v.isPlainObject(n)&&(o.opts=l(o.opts,n)),v.fancybox.isMobile&&(o.opts=l(o.opts,o.opts.mobile)),o.id=o.opts.id||++i,o.currIndex=parseInt(o.opts.index,10)||0,o.prevIndex=null,o.prevPos=null,o.currPos=0,o.firstRun=!0,// All group items
o.group=[],// Existing slides (for current, next and previous gallery items)
o.slides={},// Create group elements
o.addContent(e),o.group.length&&(// Save last active element
o.$lastFocus=v(c.activeElement).trigger("blur"),o.init())},e;// Few useful variables and methods
// ================================
v.extend(d.prototype,{
// Create DOM structure
// ====================
init:function t(){var e=this,n,a=e.group[e.currIndex].opts,o=v.fancybox.scrollbarWidth,i,s,r;// Hide scrollbars
// ===============
v.fancybox.getInstance()||!1===a.hideScrollbar||(v("body").addClass("fancybox-active"),!v.fancybox.isMobile&&c.body.scrollHeight>u.innerHeight&&(o===m&&(i=v('<div style="width:100px;height:100px;overflow:scroll;" />').appendTo("body"),o=v.fancybox.scrollbarWidth=i[0].offsetWidth-i[0].clientWidth,i.remove()),v("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar { margin-right: '+o+"px; }</style>"),v("body").addClass("compensate-for-scrollbar"))),// Build html markup and set references
// ====================================
// Build html code for buttons and insert into main template
r="",v.each(a.buttons,function(t,e){r+=a.btnTpl[e]||""}),// Create markup from base template, it will be initially hidden to
// avoid unnecessary work like painting while initializing is not complete
s=v(e.translate(e,a.baseTpl.replace("{{buttons}}",r).replace("{{arrows}}",a.btnTpl.arrowLeft+a.btnTpl.arrowRight))).attr("id","fancybox-container-"+e.id).addClass("fancybox-is-hidden").addClass(a.baseClass).data("FancyBox",e).appendTo(a.parentEl),// Create object holding references to jQuery wrapped nodes
e.$refs={container:s},["bg","inner","infobar","toolbar","stage","caption","navigation"].forEach(function(t){e.$refs[t]=s.find(".fancybox-"+t)}),e.trigger("onInit"),// Enable events, deactive previous instances
e.activate(),// Build slides, load and reveal content
e.jumpTo(e.currIndex)},
// Simple i18n support - replaces object keys found in template
// with corresponding values
// ============================================================
translate:function t(e,n){var a=e.opts.i18n[e.opts.lang];return n.replace(/\{\{(\w+)\}\}/g,function(t,e){var n=a[e];return n===m?t:n})},
// Populate current group with fresh content
// Check if each object has valid type and content
// ===============================================
addContent:function t(e){var l=this,n=v.makeArray(e),a;v.each(n,function(t,e){var n={},a={},o,i,s,r,c;// Step 1 - Make sure we have an object
// ====================================
v.isPlainObject(e)?a=(
// We probably have manual usage here, something like
// $.fancybox.open( [ { src : "image.jpg", type : "image" } ] )
n=e).opts||e:"object"===v.type(e)&&v(e).length?(
// Here we probably have jQuery collection returned by some selector
// Support attributes like `data-options='{"touch" : false}'` and `data-touch='false'`
a=(o=v(e)).data()||{},// Here we store clicked element
(a=v.extend(!0,{},a,a.options)).$orig=o,n.src=l.opts.src||a.src||o.attr("href"),// Assume that simple syntax is used, for example:
//   `$.fancybox.open( $("#test"), {} );`
n.type||n.src||(n.type="inline",n.src=e)):
// Assume we have a simple html code, for example:
//   $.fancybox.open( '<div><h1>Hi!</h1></div>' );
n={type:"html",src:e+""},// Each gallery object has full collection of options
n.opts=v.extend(!0,{},l.opts,a),// Do not merge buttons array
v.isArray(a.buttons)&&(n.opts.buttons=a.buttons),// Step 2 - Make sure we have content type, if not - try to guess
// ==============================================================
i=n.type||n.opts.type,r=n.src||"",!i&&r&&((s=r.match(/\.(mp4|mov|ogv)((\?|#).*)?$/i))?(i="video",n.opts.videoFormat||(n.opts.videoFormat="video/"+("ogv"===s[1]?"ogg":s[1]))):r.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i)?i="image":r.match(/\.(pdf)((\?|#).*)?$/i)?i="iframe":"#"===r.charAt(0)&&(i="inline")),i?n.type=i:l.trigger("objectNeedsType",n),n.contentType||(n.contentType=-1<v.inArray(n.type,["html","inline","ajax"])?"html":n.type),// Step 3 - Some adjustments
// =========================
n.index=l.group.length,"auto"==n.opts.smallBtn&&(n.opts.smallBtn=-1<v.inArray(n.type,["html","inline","ajax"])),"auto"===n.opts.toolbar&&(n.opts.toolbar=!n.opts.smallBtn),// Find thumbnail image
n.opts.$trigger&&n.index===l.opts.index&&(n.opts.$thumb=n.opts.$trigger.find("img:first")),n.opts.$thumb&&n.opts.$thumb.length||!n.opts.$orig||(n.opts.$thumb=n.opts.$orig.find("img:first")),// "caption" is a "special" option, it can be used to customize caption per gallery item ..
"function"===v.type(n.opts.caption)&&(n.opts.caption=n.opts.caption.apply(e,[l,n])),"function"===v.type(l.opts.caption)&&(n.opts.caption=l.opts.caption.apply(e,[l,n])),// Make sure we have caption as a string or jQuery object
n.opts.caption instanceof v||(n.opts.caption=n.opts.caption===m?"":n.opts.caption+""),// Check if url contains "filter" used to filter the content
// Example: "ajax.html #something"
"ajax"===n.type&&1<(c=r.split(/\s+/,2)).length&&(n.src=c.shift(),n.opts.filter=c.shift()),// Hide all buttons and disable interactivity for modal items
n.opts.modal&&(n.opts=v.extend(!0,n.opts,{
// Remove buttons
infobar:0,toolbar:0,smallBtn:0,
// Disable keyboard navigation
keyboard:0,
// Disable some modules
slideShow:0,fullScreen:0,thumbs:0,touch:0,
// Disable click event handlers
clickContent:!1,clickSlide:!1,clickOutside:!1,dblclickContent:!1,dblclickSlide:!1,dblclickOutside:!1})),// Step 4 - Add processed object to group
// ======================================
l.group.push(n)}),// Update controls if gallery is already opened
Object.keys(l.slides).length&&(l.updateControls(),(// Update thumbnails, if needed
a=l.Thumbs)&&a.isActive&&(a.create(),a.focus()))},
// Attach an event handler functions for:
//   - navigation buttons
//   - browser scrolling, resizing;
//   - focusing
//   - keyboard
//   - detect idle
// ======================================
addEvents:function t(){var a=this;a.removeEvents(),// Make navigation elements clickable
a.$refs.container.on("click.fb-close","[data-fancybox-close]",function(t){t.stopPropagation(),t.preventDefault(),a.close(t)}).on("touchstart.fb-prev click.fb-prev","[data-fancybox-prev]",function(t){t.stopPropagation(),t.preventDefault(),a.previous()}).on("touchstart.fb-next click.fb-next","[data-fancybox-next]",function(t){t.stopPropagation(),t.preventDefault(),a.next()}).on("click.fb","[data-fancybox-zoom]",function(t){
// Click handler for zoom button
a[a.isScaledDown()?"scaleToActual":"scaleToFit"]()}),// Handle page scrolling and browser resizing
o.on("orientationchange.fb resize.fb",function(t){t&&t.originalEvent&&"resize"===t.originalEvent.type?f(function(){a.update()}):(a.$refs.stage.hide(),setTimeout(function(){a.$refs.stage.show(),a.update()},v.fancybox.isMobile?600:250))}),// Trap keyboard focus inside of the modal, so the user does not accidentally tab outside of the modal
// (a.k.a. "escaping the modal")
r.on("focusin.fb",function(t){var e=v.fancybox?v.fancybox.getInstance():null;e.isClosing||!e.current||!e.current.opts.trapFocus||v(t.target).hasClass("fancybox-container")||v(t.target).is(c)||e&&"fixed"!==v(t.target).css("position")&&!e.$refs.container.has(t.target).length&&(t.stopPropagation(),e.focus())}),// Enable keyboard navigation
r.on("keydown.fb",function(t){var e=a.current,n=t.keyCode||t.which;if(e&&e.opts.keyboard&&!(t.ctrlKey||t.altKey||t.shiftKey||v(t.target).is("input")||v(t.target).is("textarea")))// Backspace and Esc keys
return 8===n||27===n?(t.preventDefault(),void a.close(t)):// Left arrow and Up arrow
37===n||38===n?(t.preventDefault(),void a.previous()):// Righ arrow and Down arrow
39===n||40===n?(t.preventDefault(),void a.next()):void a.trigger("afterKeydown",t,n)}),// Hide controls after some inactivity period
a.group[a.currIndex].opts.idleTime&&(a.idleSecondsCounter=0,r.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle",function(t){a.idleSecondsCounter=0,a.isIdle&&a.showControls(),a.isIdle=!1}),a.idleInterval=u.setInterval(function(){a.idleSecondsCounter++,a.idleSecondsCounter>=a.group[a.currIndex].opts.idleTime&&!a.isDragging&&(a.isIdle=!0,a.idleSecondsCounter=0,a.hideControls())},1e3))},
// Remove events added by the core
// ===============================
removeEvents:function t(){var e=this;o.off("orientationchange.fb resize.fb"),r.off("focusin.fb keydown.fb .fb-idle"),this.$refs.container.off(".fb-close .fb-prev .fb-next"),e.idleInterval&&(u.clearInterval(e.idleInterval),e.idleInterval=null)},
// Change to previous gallery item
// ===============================
previous:function t(e){return this.jumpTo(this.currPos-1,e)},
// Change to next gallery item
// ===========================
next:function t(e){return this.jumpTo(this.currPos+1,e)},
// Switch to selected gallery item
// ===============================
jumpTo:function t(e,a){var o=this,n=o.group.length,i,s,r,c,l,d,u;if(!(o.isDragging||o.isClosing||o.isAnimating&&o.firstRun)){if(e=parseInt(e,10),!(// Should loop?
s=o.current?o.current.opts.loop:o.opts.loop)&&(e<0||n<=e))return!1;if(i=o.firstRun=!Object.keys(o.slides).length,!(n<2&&!i&&o.isDragging)){// Fresh start - reveal container, current slide and start loading content
if(c=o.current,o.prevIndex=o.currIndex,o.prevPos=o.currPos,// Create slides
r=o.createSlide(e),1<n&&((s||0<r.index)&&o.createSlide(e-1),(s||r.index<n-1)&&o.createSlide(e+1)),o.current=r,o.currIndex=r.index,o.currPos=r.pos,o.trigger("beforeShow",i),o.updateControls(),d=v.fancybox.getTranslate(r.$slide),r.isMoved=(0!==d.left||0!==d.top)&&!r.$slide.hasClass("fancybox-animated"),// Validate duration length
r.forcedDuration=m,v.isNumeric(a)?r.forcedDuration=a:a=r.opts[i?"animationDuration":"transitionDuration"],a=parseInt(a,10),i)return r.opts.animationEffect&&a&&o.$refs.container.css("transition-duration",a+"ms"),o.$refs.container.removeClass("fancybox-is-hidden"),g(o.$refs.container),o.$refs.container.addClass("fancybox-is-open"),g(o.$refs.container),// Make current slide visible
r.$slide.addClass("fancybox-slide--previous"),// Attempt to load content into slide;
// at this point image would start loading, but inline/html content would load immediately
o.loadSlide(r),r.$slide.removeClass("fancybox-slide--previous").addClass("fancybox-slide--current"),void o.preload("image");// Clean up
v.each(o.slides,function(t,e){v.fancybox.stop(e.$slide)}),// Make current that slide is visible even if content is still loading
r.$slide.removeClass("fancybox-slide--next fancybox-slide--previous").addClass("fancybox-slide--current"),// If slides have been dragged, animate them to correct position
r.isMoved?(l=Math.round(r.$slide.width()),v.each(o.slides,function(t,e){var n=e.pos-r.pos;v.fancybox.animate(e.$slide,{top:0,left:n*l+n*e.opts.gutter},a,function(){e.$slide.removeAttr("style").removeClass("fancybox-slide--next fancybox-slide--previous"),e.pos===o.currPos&&(r.isMoved=!1,o.complete())})})):o.$refs.stage.children().removeAttr("style"),// Start transition that reveals current content
// or wait when it will be loaded
r.isLoaded?o.revealContent(r):o.loadSlide(r),o.preload("image"),c.pos!==r.pos&&(// Handle previous slide
// =====================
u="fancybox-slide--"+(c.pos>r.pos?"next":"previous"),c.$slide.removeClass("fancybox-slide--complete fancybox-slide--current fancybox-slide--next fancybox-slide--previous"),c.isComplete=!1,a&&(r.isMoved||r.opts.transitionEffect)&&(r.isMoved?c.$slide.addClass(u):(u="fancybox-animated "+u+" fancybox-fx-"+r.opts.transitionEffect,v.fancybox.animate(c.$slide,u,a,function(){c.$slide.removeClass(u).removeAttr("style")}))))}}},
// Create new "slide" element
// These are gallery items  that are actually added to DOM
// =======================================================
createSlide:function t(e){var n=this,a,o;return o=(o=e%n.group.length)<0?n.group.length+o:o,!n.slides[e]&&n.group[o]&&(a=v('<div class="fancybox-slide"></div>').appendTo(n.$refs.stage),n.slides[e]=v.extend(!0,{},n.group[o],{pos:e,$slide:a,isLoaded:!1}),n.updateSlide(n.slides[e])),n.slides[e]},
// Scale image to the actual size of the image;
// x and y values should be relative to the slide
// ==============================================
scaleToActual:function t(e,n,a){var o=this,i=o.current,s=i.$content,r=v.fancybox.getTranslate(i.$slide).width,c=v.fancybox.getTranslate(i.$slide).height,l=i.width,d=i.height,u,p,f,h,g;!o.isAnimating&&s&&"image"==i.type&&i.isLoaded&&!i.hasError&&(v.fancybox.stop(s),o.isAnimating=!0,e=e===m?.5*r:e,n=n===m?.5*c:n,(u=v.fancybox.getTranslate(s)).top-=v.fancybox.getTranslate(i.$slide).top,u.left-=v.fancybox.getTranslate(i.$slide).left,h=l/u.width,g=d/u.height,// Get center position for original image
p=.5*r-.5*l,f=.5*c-.5*d,// Make sure image does not move away from edges
r<l&&(0<(p=u.left*h-(e*h-e))&&(p=0),p<r-l&&(p=r-l)),c<d&&(0<(f=u.top*g-(n*g-n))&&(f=0),f<c-d&&(f=c-d)),o.updateCursor(l,d),v.fancybox.animate(s,{top:f,left:p,scaleX:h,scaleY:g},a||330,function(){o.isAnimating=!1}),// Stop slideshow
o.SlideShow&&o.SlideShow.isActive&&o.SlideShow.stop())},
// Scale image to fit inside parent element
// ========================================
scaleToFit:function t(e){var n=this,a=n.current,o=a.$content,i;!n.isAnimating&&o&&"image"==a.type&&a.isLoaded&&!a.hasError&&(v.fancybox.stop(o),n.isAnimating=!0,i=n.getFitPos(a),n.updateCursor(i.width,i.height),v.fancybox.animate(o,{top:i.top,left:i.left,scaleX:i.width/o.width(),scaleY:i.height/o.height()},e||330,function(){n.isAnimating=!1}))},
// Calculate image size to fit inside viewport
// ===========================================
getFitPos:function t(e){var n=this,a=e.$content,o=e.width||e.opts.width,i=e.height||e.opts.height,s,r,c,l,d,u={};return!!(e.isLoaded&&a&&a.length)&&(l={top:parseInt(e.$slide.css("paddingTop"),10),right:parseInt(e.$slide.css("paddingRight"),10),bottom:parseInt(e.$slide.css("paddingBottom"),10),left:parseInt(e.$slide.css("paddingLeft"),10)},// We can not use $slide width here, because it can have different diemensions while in transiton
s=parseInt(n.$refs.stage.width(),10)-(l.left+l.right),r=parseInt(n.$refs.stage.height(),10)-(l.top+l.bottom),o&&i||(o=s,i=r),c=Math.min(1,s/o,r/i),// Use floor rounding to make sure it really fits
o=Math.floor(c*o),i=Math.floor(c*i),"image"===e.type?(u.top=Math.floor(.5*(r-i))+l.top,u.left=Math.floor(.5*(s-o))+l.left):"video"===e.contentType&&(o/(
// Force aspect ratio for the video
// "I say the whole world must learn of our peaceful ways… by force!"
d=e.opts.width&&e.opts.height?o/i:e.opts.ratio||16/9)<i?i=o/d:i*d<o&&(o=i*d)),u.width=o,u.height=i,u)},
// Update content size and position for all slides
// ==============================================
update:function t(){var n=this;v.each(n.slides,function(t,e){n.updateSlide(e)})},
// Update slide content position and size
// ======================================
updateSlide:function t(e,n){var a=this,o=e&&e.$content,i=e.width||e.opts.width,s=e.height||e.opts.height;o&&(i||s||"video"===e.contentType)&&!e.hasError&&(v.fancybox.stop(o),v.fancybox.setTranslate(o,a.getFitPos(e)),e.pos===a.currPos&&(a.isAnimating=!1,a.updateCursor())),e.$slide.trigger("refresh"),a.$refs.toolbar.toggleClass("compensate-for-scrollbar",e.$slide.get(0).scrollHeight>e.$slide.get(0).clientHeight),a.trigger("onUpdate",e)},
// Horizontally center slide
// =========================
centerSlide:function t(e,n){var a=this,o,i;a.current&&(o=Math.round(e.$slide.width()),i=e.pos-a.current.pos,v.fancybox.animate(e.$slide,{top:0,left:i*o+i*e.opts.gutter,opacity:1},n===m?0:n,null,!1))},
// Update cursor style depending if content can be zoomed
// ======================================================
updateCursor:function t(e,n){var a=this,o=a.current,i=a.$refs.container.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-drag fancybox-can-zoomOut"),s;o&&!a.isClosing&&(s=a.isZoomable(),i.toggleClass("fancybox-is-zoomable",s),v("[data-fancybox-zoom]").prop("disabled",!s),// Set cursor to zoom in/out if click event is 'zoom'
s&&("zoom"===o.opts.clickContent||v.isFunction(o.opts.clickContent)&&"zoom"===o.opts.clickContent(o))?a.isScaledDown(e,n)?
// If image is scaled down, then, obviously, it can be zoomed to full size
i.addClass("fancybox-can-zoomIn"):o.opts.touch?
// If image size ir largen than available available and touch module is not disable,
// then user can do panning
i.addClass("fancybox-can-drag"):i.addClass("fancybox-can-zoomOut"):o.opts.touch&&"video"!==o.contentType&&i.addClass("fancybox-can-drag"))},
// Check if current slide is zoomable
// ==================================
isZoomable:function t(){var e=this,n=e.current,a;// Assume that slide is zoomable if:
//   - image is still loading
//   - actual size of the image is smaller than available area
if(n&&!e.isClosing&&"image"===n.type&&!n.hasError){if(!n.isLoaded)return!0;if(a=e.getFitPos(n),n.width>a.width||n.height>a.height)return!0}return!1},
// Check if current image dimensions are smaller than actual
// =========================================================
isScaledDown:function t(e,n){var a,o=!1,i=this.current,s=i.$content;return e!==m&&n!==m?o=e<i.width&&n<i.height:s&&(o=(o=v.fancybox.getTranslate(s)).width<i.width&&o.height<i.height),o},
// Check if image dimensions exceed parent element
// ===============================================
canPan:function t(){var e=this,n=!1,a=e.current,o;return"image"===a.type&&(o=a.$content)&&!a.hasError&&(n=e.getFitPos(a),n=1<Math.abs(o.width()-n.width)||1<Math.abs(o.height()-n.height)),n},
// Load content into the slide
// ===========================
loadSlide:function t(a){var o=this,e,n,i;if(!a.isLoading&&!a.isLoaded){// Create content depending on the type
switch(a.isLoading=!0,o.trigger("beforeLoad",a),e=a.type,(n=a.$slide).off("refresh").trigger("onReset").addClass(a.opts.slideClass),e){case"image":o.setImage(a);break;case"iframe":o.setIframe(a);break;case"html":o.setContent(a,a.src||a.content);break;case"video":o.setContent(a,'<video class="fancybox-video" controls controlsList="nodownload"><source src="'+a.src+'" type="'+a.opts.videoFormat+"\">Your browser doesn't support HTML5 video</video");break;case"inline":v(a.src).length?o.setContent(a,v(a.src)):o.setError(a);break;case"ajax":o.showLoading(a),i=v.ajax(v.extend({},a.opts.ajax.settings,{url:a.src,success:function t(e,n){"success"===n&&o.setContent(a,e)},error:function t(e,n){e&&"abort"!==n&&o.setError(a)}})),n.one("onReset",function(){i.abort()});break;default:o.setError(a);break}return!0}},
// Use thumbnail image, if possible
// ================================
setImage:function t(e){var n=this,a=e.opts.srcset||e.opts.image.srcset,o,i,s,r,c;// Check if need to show loading icon
// If we have "srcset", then we need to find first matching "src" value.
// This is necessary, because when you set an src attribute, the browser will preload the image
// before any javascript or even CSS is applied.
if(e.timouts=setTimeout(function(){var t=e.$image;!e.isLoading||t&&t[0].complete||e.hasError||n.showLoading(e)},350),a){r=u.devicePixelRatio||1,c=u.innerWidth*r,// Sort by value
(s=a.split(",").map(function(t){var a={};return t.trim().split(/\s+/).forEach(function(t,e){var n=parseInt(t.substring(0,t.length-1),10);if(0===e)return a.url=t;n&&(a.value=n,a.postfix=t[t.length-1])}),a})).sort(function(t,e){return t.value-e.value});// Ok, now we have an array of all srcset values
for(var l=0;l<s.length;l++){var d=s[l];if("w"===d.postfix&&d.value>=c||"x"===d.postfix&&d.value>=r){i=d;break}}// If not found, take the last one
!i&&s.length&&(i=s[s.length-1]),i&&(e.src=i.url,// If we have default width/height values, we can calculate height for matching source
e.width&&e.height&&"w"==i.postfix&&(e.height=e.width/e.height*i.value,e.width=i.value),e.opts.srcset=a)}// This will be wrapper containing both ghost and actual image
e.$content=v('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(e.$slide.addClass("fancybox-slide--image")),// If we have a thumbnail, we can display it while actual image is loading
// Users will not stare at black screen and actual image will appear gradually
o=e.opts.thumb||!(!e.opts.$thumb||!e.opts.$thumb.length)&&e.opts.$thumb.attr("src"),!1!==e.opts.preload&&e.opts.width&&e.opts.height&&o&&(e.width=e.opts.width,e.height=e.opts.height,e.$ghost=v("<img />").one("error",function(){v(this).remove(),e.$ghost=null}).one("load",function(){n.afterLoad(e)}).addClass("fancybox-image").appendTo(e.$content).attr("src",o)),// Start loading actual image
n.setBigImage(e)},
// Create full-size image
// ======================
setBigImage:function t(e){var n=this,a=v("<img />");e.$image=a.one("error",function(){n.setError(e)}).one("load",function(){var t;e.$ghost||(n.resolveImageSlideSize(e,this.naturalWidth,this.naturalHeight),n.afterLoad(e)),// Clear timeout that checks if loading icon needs to be displayed
e.timouts&&(clearTimeout(e.timouts),e.timouts=null),n.isClosing||(e.opts.srcset&&((t=e.opts.sizes)&&"auto"!==t||(t=(1<e.width/e.height&&1<o.width()/o.height()?"100":Math.round(e.width/e.height*100))+"vw"),a.attr("sizes",t).attr("srcset",e.opts.srcset)),// Hide temporary image after some delay
e.$ghost&&setTimeout(function(){e.$ghost&&!n.isClosing&&e.$ghost.hide()},Math.min(300,Math.max(1e3,e.height/1600))),n.hideLoading(e))}).addClass("fancybox-image").attr("src",e.src).appendTo(e.$content),(a[0].complete||"complete"==a[0].readyState)&&a[0].naturalWidth&&a[0].naturalHeight?a.trigger("load"):a[0].error&&a.trigger("error")},
// Computes the slide size from image size and maxWidth/maxHeight
// ==============================================================
resolveImageSlideSize:function t(e,n,a){var o=parseInt(e.opts.width,10),i=parseInt(e.opts.height,10);// Sets the default values from the image
e.width=n,e.height=a,0<o&&(e.width=o,e.height=Math.floor(o*a/n)),0<i&&(e.width=Math.floor(i*n/a),e.height=i)},
// Create iframe wrapper, iframe and bindings
// ==========================================
setIframe:function t(i){var e=this,s=i.opts.iframe,n=i.$slide,r;i.$content=v('<div class="fancybox-content'+(s.preload?" fancybox-is-hidden":"")+'"></div>').css(s.css).appendTo(n),n.addClass("fancybox-slide--"+i.contentType),i.$iframe=r=v(s.tpl.replace(/\{rnd\}/g,(new Date).getTime())).attr(s.attr).appendTo(i.$content),s.preload?(e.showLoading(i),// Unfortunately, it is not always possible to determine if iframe is successfully loaded
// (due to browser security policy)
r.on("load.fb error.fb",function(t){this.isReady=1,i.$slide.trigger("refresh"),e.afterLoad(i)}),// Recalculate iframe content size
// ===============================
n.on("refresh.fb",function(){var t=i.$content,e=s.css.width,n=s.css.height,a,o;if(1===r[0].isReady){try{o=(a=r.contents()).find("body")}catch(t){}// Calculate contnet dimensions if it is accessible
o&&o.length&&o.children().length&&(t.css({width:"",height:""}),e===m&&(e=Math.ceil(Math.max(o[0].clientWidth,o.outerWidth(!0)))),e&&t.width(e),n===m&&(n=Math.ceil(Math.max(o[0].clientHeight,o.outerHeight(!0)))),n&&t.height(n)),t.removeClass("fancybox-is-hidden")}})):this.afterLoad(i),r.attr("src",i.src),// Remove iframe if closing or changing gallery item
n.one("onReset",function(){
// This helps IE not to throw errors when closing
try{v(this).find("iframe").hide().unbind().attr("src","//about:blank")}catch(t){}v(this).off("refresh.fb").empty(),i.isLoaded=!1})},
// Wrap and append content to the slide
// ======================================
setContent:function t(e,n){var a=this;a.isClosing||(a.hideLoading(e),e.$content&&v.fancybox.stop(e.$content),e.$slide.empty(),// If content is a jQuery object, then it will be moved to the slide.
// The placeholder is created so we will know where to put it back.
s(n)&&n.parent().length?(
// Make sure content is not already moved to fancyBox
n.parent().parent(".fancybox-slide--inline").trigger("onReset"),// Create temporary element marking original place of the content
e.$placeholder=v("<div>").hide().insertAfter(n),// Make sure content is visible
n.css("display","inline-block")):e.hasError||(
// If content is just a plain text, try to convert it to html
"string"===v.type(n)&&3===(n=v("<div>").append(v.trim(n)).contents())[0].nodeType&&(n=v("<div>").html(n)),// If "filter" option is provided, then filter content
e.opts.filter&&(n=v("<div>").html(n).find(e.opts.filter))),e.$slide.one("onReset",function(){
// Pause all html5 video/audio
v(this).find("video,audio").trigger("pause"),// Put content back
e.$placeholder&&(e.$placeholder.after(n.hide()).remove(),e.$placeholder=null),// Remove custom close button
e.$smallBtn&&(e.$smallBtn.remove(),e.$smallBtn=null),// Remove content and mark slide as not loaded
e.hasError||(v(this).empty(),e.isLoaded=!1)}),v(n).appendTo(e.$slide),v(n).is("video,audio")&&(v(n).addClass("fancybox-video"),v(n).wrap("<div></div>"),e.contentType="video",e.opts.width=e.opts.width||v(n).attr("width"),e.opts.height=e.opts.height||v(n).attr("height")),e.$content=e.$slide.children().filter("div,form,main,video,audio").first().addClass("fancybox-content"),e.$slide.addClass("fancybox-slide--"+e.contentType),this.afterLoad(e))},
// Display error message
// =====================
setError:function t(e){e.hasError=!0,e.$slide.trigger("onReset").removeClass("fancybox-slide--"+e.contentType).addClass("fancybox-slide--error"),e.contentType="html",this.setContent(e,this.translate(e,e.opts.errorTpl)),e.pos===this.currPos&&(this.isAnimating=!1)},
// Show loading icon inside the slide
// ==================================
showLoading:function t(e){var n=this;(e=e||n.current)&&!e.$spinner&&(e.$spinner=v(n.translate(n,n.opts.spinnerTpl)).appendTo(e.$slide))},
// Remove loading icon from the slide
// ==================================
hideLoading:function t(e){var n;(e=e||this.current)&&e.$spinner&&(e.$spinner.remove(),delete e.$spinner)},
// Adjustments after slide content has been loaded
// ===============================================
afterLoad:function t(e){var n=this;n.isClosing||(e.isLoading=!1,e.isLoaded=!0,n.trigger("afterLoad",e),n.hideLoading(e),e.pos===n.currPos&&n.updateCursor(),!e.opts.smallBtn||e.$smallBtn&&e.$smallBtn.length||(e.$smallBtn=v(n.translate(e,e.opts.btnTpl.smallBtn)).prependTo(e.$content)),e.opts.protect&&e.$content&&!e.hasError&&(
// Disable right click
e.$content.on("contextmenu.fb",function(t){return 2==t.button&&t.preventDefault(),!0}),// Add fake element on top of the image
// This makes a bit harder for user to select image
"image"===e.type&&v('<div class="fancybox-spaceball"></div>').appendTo(e.$content)),n.revealContent(e))},
// Make content visible
// This method is called right after content has been loaded or
// user navigates gallery and transition should start
// ============================================================
revealContent:function t(e){var n=this,a=e.$slide,o=!1,i=!1,s,r,c,l;// Zoom animation
// ==============
return s=e.opts[n.firstRun?"animationEffect":"transitionEffect"],c=e.opts[n.firstRun?"animationDuration":"transitionDuration"],c=parseInt(e.forcedDuration===m?c:e.forcedDuration,10),// Do not animate if revealing the same slide
e.pos===n.currPos&&(e.isComplete?s=!1:n.isAnimating=!0),!e.isMoved&&e.pos===n.currPos&&c||(s=!1),// Check if can zoom
"zoom"===s&&(e.pos===n.currPos&&c&&"image"===e.type&&!e.hasError&&(i=n.getThumbPos(e))?o=n.getFitPos(e):s="fade"),"zoom"===s?(o.scaleX=o.width/i.width,o.scaleY=o.height/i.height,"auto"==(// Check if we need to animate opacity
l=e.opts.zoomOpacity)&&(l=.1<Math.abs(e.width/e.height-i.width/i.height)),l&&(i.opacity=.1,o.opacity=1),// Draw image at start position
v.fancybox.setTranslate(e.$content.removeClass("fancybox-is-hidden"),i),g(e.$content),void// Start animation
v.fancybox.animate(e.$content,o,c,function(){n.isAnimating=!1,n.complete()})):(// Simply show content
// ===================
n.updateSlide(e),s?(v.fancybox.stop(a),r="fancybox-animated fancybox-slide--"+(e.pos>=n.prevPos?"next":"previous")+" fancybox-fx-"+s,a.removeAttr("style").removeClass("fancybox-slide--current fancybox-slide--next fancybox-slide--previous").addClass(r),e.$content.removeClass("fancybox-is-hidden"),// Force reflow for CSS3 transitions
g(a),void v.fancybox.animate(a,"fancybox-slide--current",c,function(t){a.removeClass(r).removeAttr("style"),e.pos===n.currPos&&n.complete()},!0)):(g(a),e.$content.removeClass("fancybox-is-hidden"),void(e.pos===n.currPos&&n.complete())))},
// Check if we can and have to zoom from thumbnail
//================================================
getThumbPos:function t(e){var n=this,a=!1,o=e.opts.$thumb,i=o&&o.length&&o[0].ownerDocument===c?o.offset():0,s,r;// Check if element is inside the viewport by at least 1 pixel
return i&&function t(e){for(var n=e[0],a=n.getBoundingClientRect(),o=[],i;null!==n.parentElement;)"hidden"!==v(n.parentElement).css("overflow")&&"auto"!==v(n.parentElement).css("overflow")||o.push(n.parentElement.getBoundingClientRect()),n=n.parentElement;return(i=o.every(function(t){var e=Math.min(a.right,t.right)-Math.max(a.left,t.left),n=Math.min(a.bottom,t.bottom)-Math.max(a.top,t.top);return 0<e&&0<n}))&&0<a.bottom&&0<a.right&&a.left<v(u).width()&&a.top<v(u).height()}(o)&&(s=n.$refs.stage.offset(),a={top:i.top-s.top+parseFloat(o.css("border-top-width")||0),left:i.left-s.left+parseFloat(o.css("border-left-width")||0),width:o.width(),height:o.height(),scaleX:1,scaleY:1}),a},
// Final adjustments after current gallery item is moved to position
// and it`s content is loaded
// ==================================================================
complete:function t(){var n=this,e=n.current,a={};!e.isMoved&&e.isLoaded&&(e.isComplete||(e.isComplete=!0,e.$slide.siblings().trigger("onReset"),n.preload("inline"),// Trigger any CSS3 transiton inside the slide
g(e.$slide),e.$slide.addClass("fancybox-slide--complete"),// Remove unnecessary slides
v.each(n.slides,function(t,e){e.pos>=n.currPos-1&&e.pos<=n.currPos+1?a[e.pos]=e:e&&(v.fancybox.stop(e.$slide),e.$slide.off().remove())}),n.slides=a),n.isAnimating=!1,n.updateCursor(),n.trigger("afterShow"),// Play first html5 video/audio
e.$slide.find("video,audio").filter(":visible:first").trigger("play"),// Try to focus on the first focusable element
(v(c.activeElement).is("[disabled]")||e.opts.autoFocus&&"image"!=e.type&&"iframe"!==e.type)&&n.focus())},
// Preload next and previous slides
// ================================
preload:function t(e){var n=this,a=n.slides[n.currPos+1],o=n.slides[n.currPos-1];a&&a.type===e&&n.loadSlide(a),o&&o.type===e&&n.loadSlide(o)},
// Try to find and focus on the first focusable element
// ====================================================
focus:function t(){var e=this.current,n;this.isClosing||e&&e.isComplete&&e.$content&&(
// Look for first input with autofocus attribute
(n=e.$content.find("input[autofocus]:enabled:visible:first")).length||(n=e.$content.find("button,:input,[tabindex],a").filter(":enabled:visible:first")),(n=n&&n.length?n:e.$content).trigger("focus"))},
// Activates current instance - brings container to the front and enables keyboard,
// notifies other instances about deactivating
// =================================================================================
activate:function t(){var e=this;// Deactivate all instances
v(".fancybox-container").each(function(){var t=v(this).data("FancyBox");// Skip self and closing instances
t&&t.id!==e.id&&!t.isClosing&&(t.trigger("onDeactivate"),t.removeEvents(),t.isVisible=!1)}),e.isVisible=!0,(e.current||e.isIdle)&&(e.update(),e.updateControls()),e.trigger("onActivate"),e.addEvents()},
// Start closing procedure
// This will start "zoom-out" animation if needed and clean everything up afterwards
// =================================================================================
close:function t(e,n){var a=this,o=a.current,i,s,r,c,l,d,u,p=function t(){a.cleanUp(e)};return!a.isClosing&&(// If beforeClose callback prevents closing, make sure content is centered
!(a.isClosing=!0)===a.trigger("beforeClose",e)?(a.isClosing=!1,f(function(){a.update()}),!1):(// Remove all events
// If there are multiple instances, they will be set again by "activate" method
a.removeEvents(),o.timouts&&clearTimeout(o.timouts),r=o.$content,i=o.opts.animationEffect,s=v.isNumeric(n)?n:i?o.opts.animationDuration:0,// Remove other slides
o.$slide.off(h).removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"),o.$slide.siblings().trigger("onReset").remove(),// Trigger animations
s&&a.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing"),// Clean up
a.hideLoading(o),a.hideControls(),a.updateCursor(),// Check if possible to zoom-out
"zoom"!==i||!0!==e&&r&&s&&"image"===o.type&&!o.hasError&&(u=a.getThumbPos(o))||(i="fade"),"zoom"===i?(v.fancybox.stop(r),d={top:(c=v.fancybox.getTranslate(r)).top,left:c.left,scaleX:c.width/u.width,scaleY:c.height/u.height,width:u.width,height:u.height},"auto"==(// Check if we need to animate opacity
l=o.opts.zoomOpacity)&&(l=.1<Math.abs(o.width/o.height-u.width/u.height)),l&&(u.opacity=0),v.fancybox.setTranslate(r,d),g(r),v.fancybox.animate(r,u,s,p)):i&&s?
// If skip animation
!0===e?setTimeout(p,s):v.fancybox.animate(o.$slide.removeClass("fancybox-slide--current"),"fancybox-animated fancybox-slide--previous fancybox-fx-"+i,s,p):p(),!0))},
// Final adjustments after removing the instance
// =============================================
cleanUp:function t(e){var n=this,a=v("body"),o,i;n.current.$slide.trigger("onReset"),n.$refs.container.empty().remove(),n.trigger("afterClose",e),// Place back focus
n.$lastFocus&&n.current.opts.backFocus&&n.$lastFocus.trigger("focus"),n.current=null,(// Check if there are other instances
o=v.fancybox.getInstance())?o.activate():(a.removeClass("fancybox-active compensate-for-scrollbar"),v("#fancybox-style-noscroll").remove())},
// Call callback and trigger an event
// ==================================
trigger:function t(e,n){var a=Array.prototype.slice.call(arguments,1),o=this,i=n&&n.opts?n:o.current,s;if(i?a.unshift(i):i=o,a.unshift(o),v.isFunction(i.opts[e])&&(s=i.opts[e].apply(i,a)),!1===s)return s;"afterClose"!==e&&o.$refs?o.$refs.container.trigger(e+".fb",a):r.trigger(e+".fb",a)},
// Update infobar values, navigation button states and reveal caption
// ==================================================================
updateControls:function t(e){var n=this,a=n.current,o=a.index,i=a.opts.caption,s=n.$refs.container,r=n.$refs.caption;// Recalculate content dimensions
a.$slide.trigger("refresh"),n.$caption=i&&i.length?r.html(i):null,n.isHiddenControls||n.isIdle||n.showControls(),// Update info and navigation elements
s.find("[data-fancybox-count]").html(n.group.length),s.find("[data-fancybox-index]").html(o+1),s.find("[data-fancybox-prev]").toggleClass("disabled",!a.opts.loop&&o<=0),s.find("[data-fancybox-next]").toggleClass("disabled",!a.opts.loop&&o>=n.group.length-1),"image"===a.type?
// Re-enable buttons; update download button source
s.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href",a.opts.image.src||a.src).show():a.opts.toolbar&&s.find("[data-fancybox-download],[data-fancybox-zoom]").hide()},
// Hide toolbar and caption
// ========================
hideControls:function t(){this.isHiddenControls=!0,this.$refs.container.removeClass("fancybox-show-infobar fancybox-show-toolbar fancybox-show-caption fancybox-show-nav")},showControls:function t(){var e=this,n=e.current?e.current.opts:e.opts,a=e.$refs.container;e.isHiddenControls=!1,e.idleSecondsCounter=0,a.toggleClass("fancybox-show-toolbar",!(!n.toolbar||!n.buttons)).toggleClass("fancybox-show-infobar",!!(n.infobar&&1<e.group.length)).toggleClass("fancybox-show-nav",!!(n.arrows&&1<e.group.length)).toggleClass("fancybox-is-modal",!!n.modal),e.$caption?a.addClass("fancybox-show-caption "):a.removeClass("fancybox-show-caption")},
// Toggle toolbar and caption
// ==========================
toggleControls:function t(){this.isHiddenControls?this.showControls():this.hideControls()}}),v.fancybox={version:"3.3.5",defaults:t,
// Get current instance and execute a command.
//
// Examples of usage:
//
//   $instance = $.fancybox.getInstance();
//   $.fancybox.getInstance().jumpTo( 1 );
//   $.fancybox.getInstance( 'jumpTo', 1 );
//   $.fancybox.getInstance( function() {
//       console.info( this.currIndex );
//   });
// ======================================================
getInstance:function t(e){var n=v('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),a=Array.prototype.slice.call(arguments,1);return n instanceof d&&("string"===v.type(e)?n[e].apply(n,a):"function"===v.type(e)&&e.apply(n,a),n)},
// Create new instance
// ===================
open:function t(e,n,a){return new d(e,n,a)},
// Close current or all instances
// ==============================
close:function t(e){var n=this.getInstance();n&&(n.close(),// Try to find and close next instance
!0===e&&this.close())},
// Close all instances and unbind all events
// =========================================
destroy:function t(){this.close(!0),r.add("body").off("click.fb-start","**")},
// Try to detect mobile devices
// ============================
isMobile:c.createTouch!==m&&/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
// Detect if 'translate3d' support is available
// ============================================
use3d:(e=c.createElement("div"),u.getComputedStyle&&u.getComputedStyle(e)&&u.getComputedStyle(e).getPropertyValue("transform")&&!(c.documentMode&&c.documentMode<11)),
// Helper function to get current visual state of an element
// returns array[ top, left, horizontal-scale, vertical-scale, opacity ]
// =====================================================================
getTranslate:function t(e){var n;return!(!e||!e.length)&&{top:(n=e[0].getBoundingClientRect()).top||0,left:n.left||0,width:n.width,height:n.height,opacity:parseFloat(e.css("opacity"))}},
// Shortcut for setting "translate3d" properties for element
// Can set be used to set opacity, too
// ========================================================
setTranslate:function t(e,n){var a="",o={};if(e&&n)return n.left===m&&n.top===m||(a=(n.left===m?e.position().left:n.left)+"px, "+(n.top===m?e.position().top:n.top)+"px",a=this.use3d?"translate3d("+a+", 0px)":"translate("+a+")"),n.scaleX!==m&&n.scaleY!==m&&(a=(a.length?a+" ":"")+"scale("+n.scaleX+", "+n.scaleY+")"),a.length&&(o.transform=a),n.opacity!==m&&(o.opacity=n.opacity),n.width!==m&&(o.width=n.width),n.height!==m&&(o.height=n.height),e.css(o)},
// Simple CSS transition handler
// =============================
animate:function t(e,n,a,o,i){var s=!1;v.isFunction(a)&&(o=a,a=null),v.isPlainObject(n)||e.removeAttr("style"),v.fancybox.stop(e),e.on(h,function(t){
// Skip events from child elements and z-index change
(!t||!t.originalEvent||e.is(t.originalEvent.target)&&"z-index"!=t.originalEvent.propertyName)&&(v.fancybox.stop(e),s&&v.fancybox.setTranslate(e,s),v.isPlainObject(n)?!1===i&&e.removeAttr("style"):!0!==i&&e.removeClass(n),v.isFunction(o)&&o(t))}),v.isNumeric(a)&&e.css("transition-duration",a+"ms"),// Start animation by changing CSS properties or class name
v.isPlainObject(n)?(n.scaleX!==m&&n.scaleY!==m&&(s=v.extend({},n,{width:e.width()*n.scaleX,height:e.height()*n.scaleY,scaleX:1,scaleY:1}),delete n.width,delete n.height,e.parent().hasClass("fancybox-slide--image")&&e.parent().addClass("fancybox-is-scaling")),v.fancybox.setTranslate(e,n)):e.addClass(n),// Make sure that `transitionend` callback gets fired
e.data("timer",setTimeout(function(){e.trigger("transitionend")},a+16))},stop:function t(e){e&&e.length&&(clearTimeout(e.data("timer")),e.off("transitionend").css("transition-duration",""),e.parent().removeClass("fancybox-is-scaling"))}},v.fn.fancybox=function(t){var e;return(e=(t=t||{}).selector||!1)?
// Use body element instead of document so it executes first
v("body").off("click.fb-start",e).on("click.fb-start",e,{options:t},n):this.off("click.fb-start").on("click.fb-start",{items:this,options:t},n),this},// Self initializing plugin for all elements having `data-fancybox` attribute
// ==========================================================================
r.on("click.fb-start","[data-fancybox]",n),// Enable "trigger elements"
// =========================
r.on("click.fb-start","[data-trigger]",function(t){n(t,{$target:v('[data-fancybox="'+v(t.currentTarget).attr("data-trigger")+'"]').eq(v(t.currentTarget).attr("data-index")||0),$trigger:v(this)})})}}(window,document,window.jQuery||jQuery),// ==========================================================================
//
// Media
// Adds additional media type support
//
// ==========================================================================
function(f){// Formats matching url to final form
var h=function t(n,e,a){if(n)return a=a||"","object"===f.type(a)&&(a=f.param(a,!0)),f.each(e,function(t,e){n=n.replace("$"+t,e||"")}),a.length&&(n+=(0<n.indexOf("?")?"&":"?")+a),n},a={youtube:{matcher:/(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,params:{autoplay:1,autohide:1,fs:1,rel:0,hd:1,wmode:"transparent",enablejsapi:1,html5:1},paramPlace:8,type:"iframe",url:"//www.youtube.com/embed/$4",thumb:"//img.youtube.com/vi/$4/hqdefault.jpg"},vimeo:{matcher:/^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,params:{autoplay:1,hd:1,show_title:1,show_byline:1,show_portrait:0,fullscreen:1,api:1},paramPlace:3,type:"iframe",url:"//player.vimeo.com/video/$2"},instagram:{matcher:/(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,type:"image",url:"//$1/p/$2/media/?size=l"},
// Examples:
// http://maps.google.com/?ll=48.857995,2.294297&spn=0.007666,0.021136&t=m&z=16
// https://www.google.com/maps/@37.7852006,-122.4146355,14.65z
// https://www.google.com/maps/@52.2111123,2.9237542,6.61z?hl=en
// https://www.google.com/maps/place/Googleplex/@37.4220041,-122.0833494,17z/data=!4m5!3m4!1s0x0:0x6c296c66619367e0!8m2!3d37.4219998!4d-122.0840572
gmap_place:{matcher:/(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,type:"iframe",url:function t(e){return"//maps.google."+e[2]+"/?ll="+(e[9]?e[9]+"&z="+Math.floor(e[10])+(e[12]?e[12].replace(/^\//,"&"):""):e[12]+"").replace(/\?/,"&")+"&output="+(e[12]&&0<e[12].indexOf("layer=c")?"svembed":"embed")}},
// Examples:
// https://www.google.com/maps/search/Empire+State+Building/
// https://www.google.com/maps/search/?api=1&query=centurylink+field
// https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393
gmap_search:{matcher:/(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,type:"iframe",url:function t(e){return"//maps.google."+e[2]+"/maps?q="+e[5].replace("query=","q=").replace("api=1","")+"&output=embed"}}};// Object containing properties for each media type
f(document).on("objectNeedsType.fb",function(t,e,o){var i=o.src||"",s=!1,n,r,c,l,d,u,p;n=f.extend(!0,{},a,o.opts.media),// Look for any matching media type
f.each(n,function(t,e){if(c=i.match(e.matcher)){if(s=e.type,p=t,u={},e.paramPlace&&c[e.paramPlace]){"?"==(d=c[e.paramPlace])[0]&&(d=d.substring(1)),d=d.split("&");for(var n=0;n<d.length;++n){var a=d[n].split("=",2);2==a.length&&(u[a[0]]=decodeURIComponent(a[1].replace(/\+/g," ")))}}return l=f.extend(!0,{},e.params,o.opts[t],u),i="function"===f.type(e.url)?e.url.call(this,c,l,o):h(e.url,c,l),r="function"===f.type(e.thumb)?e.thumb.call(this,c,l,o):h(e.thumb,c),"youtube"===t?i=i.replace(/&t=((\d+)m)?(\d+)s/,function(t,e,n,a){return"&start="+((n?60*parseInt(n,10):0)+parseInt(a,10))}):"vimeo"===t&&(i=i.replace("&%23","#")),!1}}),// If it is found, then change content type and update the url
s?(o.opts.thumb||o.opts.$thumb&&o.opts.$thumb.length||(o.opts.thumb=r),"iframe"===s&&(o.opts=f.extend(!0,o.opts,{iframe:{preload:!1,attr:{scrolling:"no"}}})),f.extend(o,{type:s,src:i,origSrc:o.src,contentSource:p,contentType:"image"===s?"image":"gmap_place"==p||"gmap_search"==p?"map":"video"})):i&&(o.type=o.opts.defaultType)})}(window.jQuery||jQuery),// ==========================================================================
//
// Guestures
// Adds touch guestures, handles click and tap events
//
// ==========================================================================
function(y,r,x){var w=y.requestAnimationFrame||y.webkitRequestAnimationFrame||y.mozRequestAnimationFrame||y.oRequestAnimationFrame||// if all else fails, use setTimeout
function(t){return y.setTimeout(t,1e3/60)},S=y.cancelAnimationFrame||y.webkitCancelAnimationFrame||y.mozCancelAnimationFrame||y.oCancelAnimationFrame||function(t){y.clearTimeout(t)},d=function t(e){var n=[];for(var a in e=(e=e.originalEvent||e||y.e).touches&&e.touches.length?e.touches:e.changedTouches&&e.changedTouches.length?e.changedTouches:[e])e[a].pageX?n.push({x:e[a].pageX,y:e[a].pageY}):e[a].clientX&&n.push({x:e[a].clientX,y:e[a].clientY});return n},C=function t(e,n,a){return n&&e?"x"===a?e.x-n.x:"y"===a?e.y-n.y:Math.sqrt(Math.pow(e.x-n.x,2)+Math.pow(e.y-n.y,2)):0},c=function t(e){if(e.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio')||x.isFunction(e.get(0).onclick)||e.data("selectable"))return!0;// Check for attributes like data-fancybox-next or data-fancybox-close
for(var n=0,a=e[0].attributes,o=a.length;n<o;n++)if("data-fancybox-"===a[n].nodeName.substr(0,14))return!0;return!1},a=function t(e){var n=y.getComputedStyle(e)["overflow-y"],a=y.getComputedStyle(e)["overflow-x"],o=("scroll"===n||"auto"===n)&&e.scrollHeight>e.clientHeight,i=("scroll"===a||"auto"===a)&&e.scrollWidth>e.clientWidth;return o||i},l=function t(e){for(var n=!1;!(n=a(e.get(0)))&&(e=e.parent()).length&&!e.hasClass("fancybox-stage")&&!e.is("body"););return n},n=function t(e){var n=this;n.instance=e,n.$bg=e.$refs.bg,n.$stage=e.$refs.stage,n.$container=e.$refs.container,n.destroy(),n.$container.on("touchstart.fb.touch mousedown.fb.touch",x.proxy(n,"ontouchstart"))};n.prototype.destroy=function(){this.$container.off(".fb.touch")},n.prototype.ontouchstart=function(t){var e=this,n=x(t.target),a=e.instance,o=a.current,i=o.$content,s="touchstart"==t.type;// Do not respond to both (touch and mouse) events
// Ignore right click
if(s&&e.$container.off("mousedown.fb.touch"),(!t.originalEvent||2!=t.originalEvent.button)&&n.length&&!c(n)&&!c(n.parent())&&(n.is("img")||!(t.originalEvent.clientX>n[0].clientWidth+n.offset().left)))// Ignore clicks on the scrollbar
{// Ignore clicks while zooming or closing
if(!o||a.isAnimating||a.isClosing)return t.stopPropagation(),void t.preventDefault();e.realPoints=e.startPoints=d(t),e.startPoints.length&&(t.stopPropagation(),e.startEvent=t,e.canTap=!0,e.$target=n,e.$content=i,e.opts=o.opts.touch,e.isPanning=!1,e.isSwiping=!1,e.isZooming=!1,e.isScrolling=!1,e.startTime=(new Date).getTime(),e.distanceX=e.distanceY=e.distance=0,e.canvasWidth=Math.round(o.$slide[0].clientWidth),e.canvasHeight=Math.round(o.$slide[0].clientHeight),e.contentLastPos=null,e.contentStartPos=x.fancybox.getTranslate(e.$content)||{top:0,left:0},e.sliderStartPos=e.sliderLastPos||x.fancybox.getTranslate(o.$slide),// Since position will be absolute, but we need to make it relative to the stage
e.stagePos=x.fancybox.getTranslate(a.$refs.stage),e.sliderStartPos.top-=e.stagePos.top,e.sliderStartPos.left-=e.stagePos.left,e.contentStartPos.top-=e.stagePos.top,e.contentStartPos.left-=e.stagePos.left,x(r).off(".fb.touch").on(s?"touchend.fb.touch touchcancel.fb.touch":"mouseup.fb.touch mouseleave.fb.touch",x.proxy(e,"ontouchend")).on(s?"touchmove.fb.touch":"mousemove.fb.touch",x.proxy(e,"ontouchmove")),x.fancybox.isMobile&&r.addEventListener("scroll",e.onscroll,!0),(e.opts||a.canPan())&&(n.is(e.$stage)||e.$stage.find(n).length)?(x.fancybox.isMobile&&(l(n)||l(n.parent()))||t.preventDefault(),(1===e.startPoints.length||o.hasError)&&(e.instance.canPan()?(x.fancybox.stop(e.$content),e.$content.css("transition-duration",""),e.isPanning=!0):e.isSwiping=!0,e.$container.addClass("fancybox-controls--isGrabbing")),2===e.startPoints.length&&"image"===o.type&&(o.isLoaded||o.$ghost)&&(e.canTap=!1,e.isSwiping=!1,e.isPanning=!1,e.isZooming=!0,x.fancybox.stop(e.$content),e.$content.css("transition-duration",""),e.centerPointStartX=.5*(e.startPoints[0].x+e.startPoints[1].x)-x(y).scrollLeft(),e.centerPointStartY=.5*(e.startPoints[0].y+e.startPoints[1].y)-x(y).scrollTop(),e.percentageOfImageAtPinchPointX=(e.centerPointStartX-e.contentStartPos.left)/e.contentStartPos.width,e.percentageOfImageAtPinchPointY=(e.centerPointStartY-e.contentStartPos.top)/e.contentStartPos.height,e.startDistanceBetweenFingers=C(e.startPoints[0],e.startPoints[1]))):n.is(".fancybox-image")&&t.preventDefault())}// Ignore taping on links, buttons, input elements
},n.prototype.onscroll=function(t){var e=this;e.isScrolling=!0,r.removeEventListener("scroll",e.onscroll,!0)},n.prototype.ontouchmove=function(t){var e=this,n=x(t.target);// Make sure user has not released over iframe or disabled element
void 0===t.originalEvent.buttons||0!==t.originalEvent.buttons?e.isScrolling||!n.is(e.$stage)&&!e.$stage.find(n).length?e.canTap=!1:(e.newPoints=d(t),(e.opts||e.instance.canPan())&&e.newPoints.length&&e.newPoints.length&&(e.isSwiping&&!0===e.isSwiping||t.preventDefault(),e.distanceX=C(e.newPoints[0],e.startPoints[0],"x"),e.distanceY=C(e.newPoints[0],e.startPoints[0],"y"),e.distance=C(e.newPoints[0],e.startPoints[0]),// Skip false ontouchmove events (Chrome)
0<e.distance&&(e.isSwiping?e.onSwipe(t):e.isPanning?e.onPan():e.isZooming&&e.onZoom()))):e.ontouchend(t)},n.prototype.onSwipe=function(t){var a=this,e=a.isSwiping,n=a.sliderStartPos.left||0,o;// If direction is not yet determined
if(!0!==e)// Sticky edges
"x"==e&&(0<a.distanceX&&(a.instance.group.length<2||0===a.instance.current.index&&!a.instance.current.opts.loop)?n+=Math.pow(a.distanceX,.8):a.distanceX<0&&(a.instance.group.length<2||a.instance.current.index===a.instance.group.length-1&&!a.instance.current.opts.loop)?n-=Math.pow(-a.distanceX,.8):n+=a.distanceX),a.sliderLastPos={top:"x"==e?0:a.sliderStartPos.top+a.distanceY,left:n},a.requestId&&(S(a.requestId),a.requestId=null),a.requestId=w(function(){a.sliderLastPos&&(x.each(a.instance.slides,function(t,e){var n=e.pos-a.instance.currPos;x.fancybox.setTranslate(e.$slide,{top:a.sliderLastPos.top,left:a.sliderLastPos.left+n*a.canvasWidth+n*e.opts.gutter})}),a.$container.addClass("fancybox-is-sliding"))});else
// We need at least 10px distance to correctly calculate an angle
if(10<Math.abs(a.distance)){if(a.canTap=!1,a.instance.group.length<2&&a.opts.vertical?a.isSwiping="y":a.instance.isDragging||!1===a.opts.vertical||"auto"===a.opts.vertical&&800<x(y).width()?a.isSwiping="x":(o=Math.abs(180*Math.atan2(a.distanceY,a.distanceX)/Math.PI),a.isSwiping=45<o&&o<135?"y":"x"),a.canTap=!1,"y"===a.isSwiping&&x.fancybox.isMobile&&(l(a.$target)||l(a.$target.parent())))return void(a.isScrolling=!0);a.instance.isDragging=a.isSwiping,// Reset points to avoid jumping, because we dropped first swipes to calculate the angle
a.startPoints=a.newPoints,x.each(a.instance.slides,function(t,e){x.fancybox.stop(e.$slide),e.$slide.css("transition-duration",""),e.inTransition=!1,e.pos===a.instance.current.pos&&(a.sliderStartPos.left=x.fancybox.getTranslate(e.$slide).left-x.fancybox.getTranslate(a.instance.$refs.stage).left)}),// Stop slideshow
a.instance.SlideShow&&a.instance.SlideShow.isActive&&a.instance.SlideShow.stop()}},n.prototype.onPan=function(){var t=this;// Prevent accidental movement (sometimes, when tapping casually, finger can move a bit)
C(t.newPoints[0],t.realPoints[0])<(x.fancybox.isMobile?10:5)?t.startPoints=t.newPoints:(t.canTap=!1,t.contentLastPos=t.limitMovement(),t.requestId&&(S(t.requestId),t.requestId=null),t.requestId=w(function(){x.fancybox.setTranslate(t.$content,t.contentLastPos)}))},// Make panning sticky to the edges
n.prototype.limitMovement=function(){var t=this,e=t.canvasWidth,n=t.canvasHeight,a=t.distanceX,o=t.distanceY,i=t.contentStartPos,s=i.left,r=i.top,c=i.width,l=i.height,d,u,p,f,h,g;return h=e<c?s+a:s,g=r+o,// Slow down proportionally to traveled distance
d=Math.max(0,.5*e-.5*c),u=Math.max(0,.5*n-.5*l),p=Math.min(e-c,.5*e-.5*c),f=Math.min(n-l,.5*n-.5*l),//   ->
0<a&&d<h&&(h=d-1+Math.pow(-d+s+a,.8)||0),//    <-
a<0&&h<p&&(h=p+1-Math.pow(p-s-a,.8)||0),//   \/
0<o&&u<g&&(g=u-1+Math.pow(-u+r+o,.8)||0),//   /\
o<0&&g<f&&(g=f+1-Math.pow(f-r-o,.8)||0),{top:g,left:h}},n.prototype.limitPosition=function(t,e,n,a){var o=this,i=o.canvasWidth,s=o.canvasHeight;return t=i<n?(t=0<t?0:t)<i-n?i-n:t:Math.max(0,i/2-n/2),{top:e=s<a?(e=0<e?0:e)<s-a?s-a:e:Math.max(0,s/2-a/2),left:t}},n.prototype.onZoom=function(){var t=this,e=t.contentStartPos,n=e.width,a=e.height,o=e.left,i=e.top,s,r=C(t.newPoints[0],t.newPoints[1])/t.startDistanceBetweenFingers,c=Math.floor(n*r),l=Math.floor(a*r),d=(n-c)*t.percentageOfImageAtPinchPointX,u=(a-l)*t.percentageOfImageAtPinchPointY,p=(t.newPoints[0].x+t.newPoints[1].x)/2-x(y).scrollLeft(),f=(t.newPoints[0].y+t.newPoints[1].y)/2-x(y).scrollTop(),h=p-t.centerPointStartX,g,v,m,b={top:i+(u+(f-t.centerPointStartY)),left:o+(d+h),scaleX:r,scaleY:r};// Calculate current distance between points to get pinch ratio and new width and height
t.canTap=!1,t.newWidth=c,t.newHeight=l,t.contentLastPos=b,t.requestId&&(S(t.requestId),t.requestId=null),t.requestId=w(function(){x.fancybox.setTranslate(t.$content,t.contentLastPos)})},n.prototype.ontouchend=function(t){var e=this,n=Math.max((new Date).getTime()-e.startTime,1),a=e.isSwiping,o=e.isPanning,i=e.isZooming,s=e.isScrolling;if(e.endPoints=d(t),e.$container.removeClass("fancybox-controls--isGrabbing"),x(r).off(".fb.touch"),r.removeEventListener("scroll",e.onscroll,!0),e.requestId&&(S(e.requestId),e.requestId=null),e.isSwiping=!1,e.isPanning=!1,e.isZooming=!1,e.isScrolling=!1,e.instance.isDragging=!1,e.canTap)return e.onTap(t);e.speed=366,// Speed in px/ms
e.velocityX=e.distanceX/n*.5,e.velocityY=e.distanceY/n*.5,e.speedX=Math.max(.5*e.speed,Math.min(1.5*e.speed,1/Math.abs(e.velocityX)*e.speed)),o?e.endPanning():i?e.endZooming():e.endSwiping(a,s)},n.prototype.endSwiping=function(t,e){var n=this,a=!1,o=n.instance.group.length;n.sliderLastPos=null,// Close if swiped vertically / navigate if horizontally
"y"==t&&!e&&50<Math.abs(n.distanceY)?(
// Continue vertical movement
x.fancybox.animate(n.instance.current.$slide,{top:n.sliderStartPos.top+n.distanceY+150*n.velocityY,opacity:0},200),a=n.instance.close(!0,200)):"x"==t&&50<n.distanceX&&1<o?a=n.instance.previous(n.speedX):"x"==t&&n.distanceX<-50&&1<o&&(a=n.instance.next(n.speedX)),!1!==a||"x"!=t&&"y"!=t||(e||o<2?n.instance.centerSlide(n.instance.current,150):n.instance.jumpTo(n.instance.current.index)),n.$container.removeClass("fancybox-is-sliding")},// Limit panning from edges
// ========================
n.prototype.endPanning=function(){var t=this,e,n,a;t.contentLastPos&&(n=!1===t.opts.momentum?(e=t.contentLastPos.left,t.contentLastPos.top):(
// Continue movement
e=t.contentLastPos.left+t.velocityX*t.speed,t.contentLastPos.top+t.velocityY*t.speed),(a=t.limitPosition(e,n,t.contentStartPos.width,t.contentStartPos.height)).width=t.contentStartPos.width,a.height=t.contentStartPos.height,x.fancybox.animate(t.$content,a,330))},n.prototype.endZooming=function(){var t=this,e=t.instance.current,n,a,o,i,s=t.newWidth,r=t.newHeight;t.contentLastPos&&(n=t.contentLastPos.left,i={top:a=t.contentLastPos.top,left:n,width:s,height:r,scaleX:1,scaleY:1},// Reset scalex/scaleY values; this helps for perfomance and does not break animation
x.fancybox.setTranslate(t.$content,i),s<t.canvasWidth&&r<t.canvasHeight?t.instance.scaleToFit(150):s>e.width||r>e.height?t.instance.scaleToActual(t.centerPointStartX,t.centerPointStartY,150):(o=t.limitPosition(n,a,s,r),// Switch from scale() to width/height or animation will not work correctly
x.fancybox.setTranslate(t.$content,x.fancybox.getTranslate(t.$content)),x.fancybox.animate(t.$content,o,150)))},n.prototype.onTap=function(a){var o=this,t=x(a.target),i=o.instance,s=i.current,e=a&&d(a)||o.startPoints,r=e[0]?e[0].x-x(y).scrollLeft()-o.stagePos.left:0,c=e[0]?e[0].y-x(y).scrollTop()-o.stagePos.top:0,n,l=function t(e){var n=s.opts[e];if(x.isFunction(n)&&(n=n.apply(i,[s,a])),n)switch(n){case"close":i.close(o.startEvent);break;case"toggleControls":i.toggleControls(!0);break;case"next":i.next();break;case"nextOrClose":1<i.group.length?i.next():i.close(o.startEvent);break;case"zoom":"image"==s.type&&(s.isLoaded||s.$ghost)&&(i.canPan()?i.scaleToFit():i.isScaledDown()?i.scaleToActual(r,c):i.group.length<2&&i.close(o.startEvent));break}};// Ignore right click
if((!a.originalEvent||2!=a.originalEvent.button)&&(t.is("img")||!(r>t[0].clientWidth+t.offset().left)))// Skip if clicked on the scrollbar
{// Check where is clicked
if(t.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container"))n="Outside";else if(t.is(".fancybox-slide"))n="Slide";else{if(!i.current.$content||!i.current.$content.find(t).addBack().filter(t).length)return;// Check if this is a double tap
n="Content"}if(o.tapped){// Skip if distance between taps is too big
if(
// Stop previously created single tap
clearTimeout(o.tapped),o.tapped=null,50<Math.abs(r-o.tapX)||50<Math.abs(c-o.tapY))return this;// OK, now we assume that this is a double-tap
l("dblclick"+n)}else
// Single tap will be processed if user has not clicked second time within 300ms
// or there is no need to wait for double-tap
o.tapX=r,o.tapY=c,s.opts["dblclick"+n]&&s.opts["dblclick"+n]!==s.opts["click"+n]?o.tapped=setTimeout(function(){o.tapped=null,l("click"+n)},500):l("click"+n);return this}},x(r).on("onActivate.fb",function(t,e){e&&!e.Guestures&&(e.Guestures=new n(e))})}(window,document,window.jQuery||jQuery),// ==========================================================================
//
// SlideShow
// Enables slideshow functionality
//
// Example of usage:
// $.fancybox.getInstance().SlideShow.start()
//
// ==========================================================================
function(r,c){c.extend(!0,c.fancybox.defaults,{btnTpl:{slideShow:'<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"><svg viewBox="0 0 40 40"><path d="M13,12 L27,20 L13,27 Z" /><path d="M15,10 v19 M23,10 v19" /></svg></button>'},slideShow:{autoStart:!1,speed:3e3}});var a=function t(e){this.instance=e,this.init()};c.extend(a.prototype,{timer:null,isActive:!1,$button:null,init:function t(){var e=this;e.$button=e.instance.$refs.toolbar.find("[data-fancybox-play]").on("click",function(){e.toggle()}),(e.instance.group.length<2||!e.instance.group[e.instance.currIndex].opts.slideShow)&&e.$button.hide()},set:function t(e){var n=this;// Check if reached last element
n.instance&&n.instance.current&&(!0===e||n.instance.current.opts.loop||n.instance.currIndex<n.instance.group.length-1)?n.timer=setTimeout(function(){n.isActive&&n.instance.jumpTo((n.instance.currIndex+1)%n.instance.group.length)},n.instance.current.opts.slideShow.speed):(n.stop(),n.instance.idleSecondsCounter=0,n.instance.showControls())},clear:function t(){var e=this;clearTimeout(e.timer),e.timer=null},start:function t(){var e=this,n=e.instance.current;n&&(e.isActive=!0,e.$button.attr("title",n.opts.i18n[n.opts.lang].PLAY_STOP).removeClass("fancybox-button--play").addClass("fancybox-button--pause"),e.set(!0))},stop:function t(){var e=this,n=e.instance.current;e.clear(),e.$button.attr("title",n.opts.i18n[n.opts.lang].PLAY_START).removeClass("fancybox-button--pause").addClass("fancybox-button--play"),e.isActive=!1},toggle:function t(){var e=this;e.isActive?e.stop():e.start()}}),c(r).on({"onInit.fb":function t(e,n){n&&!n.SlideShow&&(n.SlideShow=new a(n))},"beforeShow.fb":function t(e,n,a,o){var i=n&&n.SlideShow;o?i&&a.opts.slideShow.autoStart&&i.start():i&&i.isActive&&i.clear()},"afterShow.fb":function t(e,n,a){var o=n&&n.SlideShow;o&&o.isActive&&o.set()},"afterKeydown.fb":function t(e,n,a,o,i){var s=n&&n.SlideShow;// "P" or Spacebar
!s||!a.opts.slideShow||80!==i&&32!==i||c(r.activeElement).is("button,a,input")||(o.preventDefault(),s.toggle())},"beforeClose.fb onDeactivate.fb":function t(e,n){var a=n&&n.SlideShow;a&&a.stop()}}),// Page Visibility API to pause slideshow when window is not active
c(r).on("visibilitychange",function(){var t=c.fancybox.getInstance(),e=t&&t.SlideShow;e&&e.isActive&&(r.hidden?e.clear():e.set())})}(document,window.jQuery||jQuery),// ==========================================================================
//
// FullScreen
// Adds fullscreen functionality
//
// ==========================================================================
function(i,n){// Collection of methods supported by user browser
var a=function(){for(var t=[["requestFullscreen","exitFullscreen","fullscreenElement","fullscreenEnabled","fullscreenchange","fullscreenerror"],// new WebKit
["webkitRequestFullscreen","webkitExitFullscreen","webkitFullscreenElement","webkitFullscreenEnabled","webkitfullscreenchange","webkitfullscreenerror"],// old WebKit (Safari 5.1)
["webkitRequestFullScreen","webkitCancelFullScreen","webkitCurrentFullScreenElement","webkitCancelFullScreen","webkitfullscreenchange","webkitfullscreenerror"],["mozRequestFullScreen","mozCancelFullScreen","mozFullScreenElement","mozFullScreenEnabled","mozfullscreenchange","mozfullscreenerror"],["msRequestFullscreen","msExitFullscreen","msFullscreenElement","msFullscreenEnabled","MSFullscreenChange","MSFullscreenError"]],e={},n=0;n<t.length;n++){var a=t[n];if(a&&a[1]in i){for(var o=0;o<a.length;o++)e[t[0][o]]=a[o];return e}}return!1}();// If browser does not have Full Screen API, then simply unset default button template and stop
if(a){var o={request:function t(e){(e=e||i.documentElement)[a.requestFullscreen](e.ALLOW_KEYBOARD_INPUT)},exit:function t(){i[a.exitFullscreen]()},toggle:function t(e){e=e||i.documentElement,this.isFullscreen()?this.exit():this.request(e)},isFullscreen:function t(){return Boolean(i[a.fullscreenElement])},enabled:function t(){return Boolean(i[a.fullscreenEnabled])}};n.extend(!0,n.fancybox.defaults,{btnTpl:{fullScreen:'<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fullscreen" title="{{FULL_SCREEN}}"><svg viewBox="0 0 40 40"><path d="M9,12 v16 h22 v-16 h-22 v8" /></svg></button>'},fullScreen:{autoStart:!1}}),n(i).on({"onInit.fb":function t(e,n){var a;n&&n.group[n.currIndex].opts.fullScreen?((a=n.$refs.container).on("click.fb-fullscreen","[data-fancybox-fullscreen]",function(t){t.stopPropagation(),t.preventDefault(),o.toggle()}),n.opts.fullScreen&&!0===n.opts.fullScreen.autoStart&&o.request(),// Expose API
n.FullScreen=o):n&&n.$refs.toolbar.find("[data-fancybox-fullscreen]").hide()},"afterKeydown.fb":function t(e,n,a,o,i){
// "F"
n&&n.FullScreen&&70===i&&(o.preventDefault(),n.FullScreen.toggle())},"beforeClose.fb":function t(e,n){n&&n.FullScreen&&n.$refs.container.hasClass("fancybox-is-fullscreen")&&o.exit()}}),n(i).on(a.fullscreenchange,function(){var t=o.isFullscreen(),e=n.fancybox.getInstance();e&&(
// If image is zooming, then force to stop and reposition properly
e.current&&"image"===e.current.type&&e.isAnimating&&(e.current.$content.css("transition","none"),e.isAnimating=!1,e.update(!0,!0,0)),e.trigger("onFullscreenChange",t),e.$refs.container.toggleClass("fancybox-is-fullscreen",t))})}else n&&n.fancybox&&(n.fancybox.defaults.btnTpl.fullScreen=!1)}(document,window.jQuery||jQuery),// ==========================================================================
//
// Thumbs
// Displays thumbnails in a grid
//
// ==========================================================================
function(t,s){var r="fancybox-thumbs",c=r+"-active",l=r+"-loading";// Make sure there are default values
s.fancybox.defaults=s.extend(!0,{btnTpl:{thumbs:'<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"><svg viewBox="0 0 120 120"><path d="M30,30 h14 v14 h-14 Z M50,30 h14 v14 h-14 Z M70,30 h14 v14 h-14 Z M30,50 h14 v14 h-14 Z M50,50 h14 v14 h-14 Z M70,50 h14 v14 h-14 Z M30,70 h14 v14 h-14 Z M50,70 h14 v14 h-14 Z M70,70 h14 v14 h-14 Z" /></svg></button>'},thumbs:{autoStart:!1,
// Display thumbnails on opening
hideOnClose:!0,
// Hide thumbnail grid when closing animation starts
parentEl:".fancybox-container",
// Container is injected into this element
axis:"y"}},s.fancybox.defaults);var o=function t(e){this.init(e)};s.extend(o.prototype,{$button:null,$grid:null,$list:null,isVisible:!1,isActive:!1,init:function t(e){var n=this,a,o;((n.instance=e).Thumbs=n).opts=e.group[e.currIndex].opts.thumbs,a=(// Enable thumbs if at least two group items have thumbnails
a=e.group[0]).opts.thumb||!(!a.opts.$thumb||!a.opts.$thumb.length)&&a.opts.$thumb.attr("src"),1<e.group.length&&(o=(o=e.group[1]).opts.thumb||!(!o.opts.$thumb||!o.opts.$thumb.length)&&o.opts.$thumb.attr("src")),n.$button=e.$refs.toolbar.find("[data-fancybox-thumbs]"),n.opts&&a&&o&&a&&o?(n.$button.show().on("click",function(){n.toggle()}),n.isActive=!0):n.$button.hide()},create:function t(){var e=this,n=e.instance,a=e.opts.parentEl,o=[],i;e.$grid||(
// Create main element
e.$grid=s('<div class="'+r+" "+r+"-"+e.opts.axis+'"></div>').appendTo(n.$refs.container.find(a).addBack().filter(a)),// Add "click" event that performs gallery navigation
e.$grid.on("click","li",function(){n.jumpTo(s(this).attr("data-index"))})),// Build the list
e.$list||(e.$list=s("<ul>").appendTo(e.$grid)),s.each(n.group,function(t,e){(i=e.opts.thumb||(e.opts.$thumb?e.opts.$thumb.attr("src"):null))||"image"!==e.type||(i=e.src),o.push('<li data-index="'+t+'" tabindex="0" class="'+l+'"'+(i&&i.length?' style="background-image:url('+i+')" />':"")+"></li>")}),e.$list[0].innerHTML=o.join(""),"x"===e.opts.axis&&
// Set fixed width for list element to enable horizontal scrolling
e.$list.width(parseInt(e.$grid.css("padding-right"),10)+n.group.length*e.$list.children().eq(0).outerWidth(!0))},focus:function t(e){var n=this,a=n.$list,o=n.$grid,i,s;n.instance.current&&(s=(i=a.children().removeClass(c).filter('[data-index="'+n.instance.current.index+'"]').addClass(c)).position(),// Check if need to scroll to make current thumb visible
"y"===n.opts.axis&&(s.top<0||s.top>a.height()-i.outerHeight())?a.stop().animate({scrollTop:a.scrollTop()+s.top},e):"x"===n.opts.axis&&(s.left<o.scrollLeft()||s.left>o.scrollLeft()+(o.width()-i.outerWidth()))&&a.parent().stop().animate({scrollLeft:s.left},e))},update:function t(){var e=this;e.instance.$refs.container.toggleClass("fancybox-show-thumbs",this.isVisible),e.isVisible?(e.$grid||e.create(),e.instance.trigger("onThumbsShow"),e.focus(0)):e.$grid&&e.instance.trigger("onThumbsHide"),// Update content position
e.instance.update()},hide:function t(){this.isVisible=!1,this.update()},show:function t(){this.isVisible=!0,this.update()},toggle:function t(){this.isVisible=!this.isVisible,this.update()}}),s(t).on({"onInit.fb":function t(e,n){var a;n&&!n.Thumbs&&(a=new o(n)).isActive&&!0===a.opts.autoStart&&a.show()},"beforeShow.fb":function t(e,n,a,o){var i=n&&n.Thumbs;i&&i.isVisible&&i.focus(o?0:250)},"afterKeydown.fb":function t(e,n,a,o,i){var s=n&&n.Thumbs;// "G"
s&&s.isActive&&71===i&&(o.preventDefault(),s.toggle())},"beforeClose.fb":function t(e,n){var a=n&&n.Thumbs;a&&a.isVisible&&!1!==a.opts.hideOnClose&&a.$grid.hide()}})}(document,window.jQuery||jQuery),//// ==========================================================================
//
// Share
// Displays simple form for sharing current url
//
// ==========================================================================
function(t,o){function i(t){var e={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;","/":"&#x2F;","`":"&#x60;","=":"&#x3D;"};return String(t).replace(/[&<>"'`=\/]/g,function(t){return e[t]})}o.extend(!0,o.fancybox.defaults,{btnTpl:{share:'<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}"><svg viewBox="0 0 40 40"><path d="M6,30 C8,18 19,16 23,16 L23,16 L23,10 L33,20 L23,29 L23,24 C19,24 8,27 6,30 Z"></svg></button>'},share:{url:function t(e,n){return!e.currentHash&&"inline"!==n.type&&"html"!==n.type&&(n.origSrc||n.src)||window.location},tpl:'<div class="fancybox-share"><h1>{{SHARE}}</h1><p><a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg><span>Facebook</span></a><a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg><span>Twitter</span></a><a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg><span>Pinterest</span></a></p><p><input class="fancybox-share__input" type="text" value="{{url_raw}}" /></p></div>'}}),o(t).on("click","[data-fancybox-share]",function(){var a=o.fancybox.getInstance(),t=a.current||null,e,n;t&&("function"===o.type(t.opts.share.url)&&(e=t.opts.share.url.apply(t,[a,t])),n=t.opts.share.tpl.replace(/\{\{media\}\}/g,"image"===t.type?encodeURIComponent(t.src):"").replace(/\{\{url\}\}/g,encodeURIComponent(e)).replace(/\{\{url_raw\}\}/g,i(e)).replace(/\{\{descr\}\}/g,a.$caption?encodeURIComponent(a.$caption.text()):""),o.fancybox.open({src:a.translate(a,n),type:"html",opts:{animationEffect:!1,afterLoad:function t(e,n){
// Close self if parent instance is closing
a.$refs.container.one("beforeClose.fb",function(){e.close(null,0)}),// Opening links in a popup window
n.$content.find(".fancybox-share__links a").click(function(){return window.open(this.href,"Share","width=550, height=450"),!1})}}}))})}(document,window.jQuery||jQuery),// ==========================================================================
//
// Hash
// Enables linking to each modal
//
// ==========================================================================
function(s,r,o){// Get info about gallery name and current index from url
function i(){var t=r.location.hash.substr(1),e=t.split("-"),n=1<e.length&&/^\+?\d+$/.test(e[e.length-1])&&parseInt(e.pop(-1),10)||1,a;return{hash:t,
/* Index is starting from 1 */
index:n<1?1:n,gallery:e.join("-")}}// Trigger click evnt on links to open new fancyBox instance
function e(t){var e;""!==t.gallery&&(
// If we can find element matching 'data-fancybox' atribute, then trigger click event for that.
// It should start fancyBox
e=o("[data-fancybox='"+o.escapeSelector(t.gallery)+"']").eq(t.index-1).trigger("click.fb-start"))}// Get gallery name from current instance
function c(t){var e,n;return!!t&&(""!==(n=(e=t.current?t.current.opts:t.opts).hash||(e.$orig?e.$orig.data("fancybox"):""))&&n)}// Start when DOM becomes ready
// Simple $.escapeSelector polyfill (for jQuery prior v3)
o.escapeSelector||(o.escapeSelector=function(t){var e=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g,n=function t(e,n){return n?
// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
"\0"===e?"�":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e;// Other potentially-special ASCII characters get backslash-escaped
};return(t+"").replace(e,n)}),o(function(){
// Check if user has disabled this module
!1!==o.fancybox.defaults.hash&&(// Update hash when opening/closing fancyBox
o(s).on({"onInit.fb":function t(e,n){var a,o;!1!==n.group[n.currIndex].opts.hash&&(a=i(),// Make sure gallery start index matches index from hash
(o=c(n))&&a.gallery&&o==a.gallery&&(n.currIndex=a.index-1))},"beforeShow.fb":function t(e,n,a,o){var i;a&&!1!==a.opts.hash&&(// Check if need to update window hash
i=c(n))&&(// Variable containing last hash value set by fancyBox
// It will be used to determine if fancyBox needs to close after hash change is detected
n.currentHash=i+(1<n.group.length?"-"+(a.index+1):""),// If current hash is the same (this instance most likely is opened by hashchange), then do nothing
r.location.hash!=="#"+n.currentHash&&(n.origHash||(n.origHash=r.location.hash),n.hashTimer&&clearTimeout(n.hashTimer),// Update hash
n.hashTimer=setTimeout(function(){"replaceState"in r.history?(r.history[o?"pushState":"replaceState"]({},s.title,r.location.pathname+r.location.search+"#"+n.currentHash),o&&(n.hasCreatedHistory=!0)):r.location.hash=n.currentHash,n.hashTimer=null},300)))},"beforeClose.fb":function t(e,n,a){var o;!1!==a.opts.hash&&(o=c(n),// Goto previous history entry
n.currentHash&&n.hasCreatedHistory?r.history.back():n.currentHash&&("replaceState"in r.history?r.history.replaceState({},s.title,r.location.pathname+r.location.search+(n.origHash||"")):r.location.hash=n.origHash),n.currentHash=null,clearTimeout(n.hashTimer))}}),// Check if need to start/close after url has changed
o(r).on("hashchange.fb",function(){var t=i(),a;// Find last fancyBox instance that has "hash"
o.each(o(".fancybox-container").get().reverse(),function(t,e){var n=o(e).data("FancyBox");//isClosing
if(n.currentHash)return a=n,!1}),a?
// Now, compare hash values
!a.currentHash||a.currentHash===t.gallery+"-"+t.index||1===t.index&&a.currentHash==t.gallery||(a.currentHash=null,a.close()):""!==t.gallery&&e(t)}),// Check current hash and trigger click event on matching element to start fancyBox, if needed
setTimeout(function(){o.fancybox.getInstance()||e(i())},50))})}(document,window,window.jQuery||jQuery),// ==========================================================================
//
// Wheel
// Basic mouse weheel support for gallery navigation
//
// ==========================================================================
function(t,e){var o=(new Date).getTime();e(t).on({"onInit.fb":function t(e,a,n){a.$refs.stage.on("mousewheel DOMMouseScroll wheel MozMousePixelScroll",function(t){var e=a.current,n=(new Date).getTime();a.group.length<2||!1===e.opts.wheel||"auto"===e.opts.wheel&&"image"!==e.type||(t.preventDefault(),t.stopPropagation(),e.$slide.hasClass("fancybox-animated")||(t=t.originalEvent||t,n-o<250||(o=n,a[(-t.deltaY||-t.deltaX||t.wheelDelta||-t.detail)<0?"next":"previous"]())))})}})}(document,window.jQuery||jQuery),function(h){var n=!0;//FlexSlider: Object Instance
h.flexslider=function(m,t){var b=h(m);// making variables public
//if rtl value was not passed and html is in rtl..enable it by default.
void 0===t.rtl&&"rtl"==h("html").attr("dir")&&(t.rtl=!0),b.vars=h.extend({},h.flexslider.defaults,t);var d=b.vars.namespace,y=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,u=("ontouchstart"in window||y||window.DocumentTouch&&document instanceof DocumentTouch)&&b.vars.touch,
// deprecating this idea, as devices are being released with both of these events
c="click touchend MSPointerUp keyup",l="",e,x="vertical"===b.vars.direction,w=b.vars.reverse,S=0<b.vars.itemWidth,C="fade"===b.vars.animation,p=""!==b.vars.asNavFor,f={};// Store a reference to the slider object
h.data(m,"flexslider",b),// Private slider methods
f={init:function t(){b.animating=!1,// Get current slide and make sure it is a number
b.currentSlide=parseInt(b.vars.startAt?b.vars.startAt:0,10),isNaN(b.currentSlide)&&(b.currentSlide=0),b.animatingTo=b.currentSlide,b.atEnd=0===b.currentSlide||b.currentSlide===b.last,b.containerSelector=b.vars.selector.substr(0,b.vars.selector.search(" ")),b.slides=h(b.vars.selector,b),b.container=h(b.containerSelector,b),b.count=b.slides.length,// SYNC:
b.syncExists=0<h(b.vars.sync).length,// SLIDE:
"slide"===b.vars.animation&&(b.vars.animation="swing"),b.prop=x?"top":b.vars.rtl?"marginRight":"marginLeft",b.args={},// SLIDESHOW:
b.manualPause=!1,b.stopped=!1,//PAUSE WHEN INVISIBLE
b.started=!1,b.startTimeout=null,// TOUCH/USECSS:
b.transitions=!b.vars.video&&!C&&b.vars.useCSS&&function(){var t=document.createElement("div"),e=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var n in e)if(void 0!==t.style[e[n]])return b.pfx=e[n].replace("Perspective","").toLowerCase(),b.prop="-"+b.pfx+"-transform",!0;return!1}(),// CONTROLSCONTAINER:
(b.ensureAnimationEnd="")!==b.vars.controlsContainer&&(b.controlsContainer=0<h(b.vars.controlsContainer).length&&h(b.vars.controlsContainer)),// MANUAL:
""!==b.vars.manualControls&&(b.manualControls=0<h(b.vars.manualControls).length&&h(b.vars.manualControls)),// CUSTOM DIRECTION NAV:
""!==b.vars.customDirectionNav&&(b.customDirectionNav=2===h(b.vars.customDirectionNav).length&&h(b.vars.customDirectionNav)),// RANDOMIZE:
b.vars.randomize&&(b.slides.sort(function(){return Math.round(Math.random())-.5}),b.container.empty().append(b.slides)),b.doMath(),// INIT
b.setup("init"),// CONTROLNAV:
b.vars.controlNav&&f.controlNav.setup(),// DIRECTIONNAV:
b.vars.directionNav&&f.directionNav.setup(),// KEYBOARD:
b.vars.keyboard&&(1===h(b.containerSelector).length||b.vars.multipleKeyboard)&&h(document).bind("keyup",function(t){var e=t.keyCode;if(!b.animating&&(39===e||37===e)){var n=b.vars.rtl?37===e?b.getTarget("next"):39===e&&b.getTarget("prev"):39===e?b.getTarget("next"):37===e&&b.getTarget("prev");b.flexAnimate(n,b.vars.pauseOnAction)}}),// MOUSEWHEEL:
b.vars.mousewheel&&b.bind("mousewheel",function(t,e,n,a){t.preventDefault();var o=e<0?b.getTarget("next"):b.getTarget("prev");b.flexAnimate(o,b.vars.pauseOnAction)}),// PAUSEPLAY
b.vars.pausePlay&&f.pausePlay.setup(),//PAUSE WHEN INVISIBLE
b.vars.slideshow&&b.vars.pauseInvisible&&f.pauseInvisible.init(),// SLIDSESHOW
b.vars.slideshow&&(b.vars.pauseOnHover&&b.hover(function(){b.manualPlay||b.manualPause||b.pause()},function(){b.manualPause||b.manualPlay||b.stopped||b.play()}),// initialize animation
//If we're visible, or we don't use PageVisibility API
b.vars.pauseInvisible&&f.pauseInvisible.isHidden()||(0<b.vars.initDelay?b.startTimeout=setTimeout(b.play,b.vars.initDelay):b.play())),// ASNAV:
p&&f.asNav.setup(),// TOUCH
u&&b.vars.touch&&f.touch(),// FADE&&SMOOTHHEIGHT || SLIDE:
(!C||C&&b.vars.smoothHeight)&&h(window).bind("resize orientationchange focus",f.resize),b.find("img").attr("draggable","false"),// API: start() Callback
setTimeout(function(){b.vars.start(b)},200)},asNav:{setup:function t(){b.asNav=!0,b.animatingTo=Math.floor(b.currentSlide/b.move),b.currentItem=b.currentSlide,b.slides.removeClass(d+"active-slide").eq(b.currentItem).addClass(d+"active-slide"),y?(m._slider=b).slides.each(function(){var t=this;t._gesture=new MSGesture,(t._gesture.target=t).addEventListener("MSPointerDown",function(t){t.preventDefault(),t.currentTarget._gesture&&t.currentTarget._gesture.addPointer(t.pointerId)},!1),t.addEventListener("MSGestureTap",function(t){t.preventDefault();var e=h(this),n=e.index();h(b.vars.asNavFor).data("flexslider").animating||e.hasClass("active")||(b.direction=b.currentItem<n?"next":"prev",b.flexAnimate(n,b.vars.pauseOnAction,!1,!0,!0))})}):b.slides.on(c,function(t){t.preventDefault();var e=h(this),n=e.index(),a;(a=b.vars.rtl?-1*(e.offset().right-h(b).scrollLeft()):e.offset().left-h(b).scrollLeft())<=0&&e.hasClass(d+"active-slide")?b.flexAnimate(b.getTarget("prev"),!0):h(b.vars.asNavFor).data("flexslider").animating||e.hasClass(d+"active-slide")||(b.direction=b.currentItem<n?"next":"prev",b.flexAnimate(n,b.vars.pauseOnAction,!1,!0,!0))})}},controlNav:{setup:function t(){b.manualControls?
// MANUALCONTROLS:
f.controlNav.setupManual():f.controlNav.setupPaging()},setupPaging:function t(){var e="thumbnails"===b.vars.controlNav?"control-thumbs":"control-paging",n=1,a,o;if(b.controlNavScaffold=h('<ol class="'+d+"control-nav "+d+e+'"></ol>'),1<b.pagingCount)for(var i=0;i<b.pagingCount;i++){void 0===(o=b.slides.eq(i)).attr("data-thumb-alt")&&o.attr("data-thumb-alt","");var s=""!==o.attr("data-thumb-alt")?s=' alt="'+o.attr("data-thumb-alt")+'"':"";if(a="thumbnails"===b.vars.controlNav?'<img src="'+o.attr("data-thumb")+'"'+s+"/>":'<a href="#">'+n+"</a>","thumbnails"===b.vars.controlNav&&!0===b.vars.thumbCaptions){var r=o.attr("data-thumbcaption");""!==r&&void 0!==r&&(a+='<span class="'+d+'caption">'+r+"</span>")}b.controlNavScaffold.append("<li>"+a+"</li>"),n++}// CONTROLSCONTAINER:
b.controlsContainer?h(b.controlsContainer).append(b.controlNavScaffold):b.append(b.controlNavScaffold),f.controlNav.set(),f.controlNav.active(),b.controlNavScaffold.delegate("a, img",c,function(t){if(t.preventDefault(),""===l||l===t.type){var e=h(this),n=b.controlNav.index(e);e.hasClass(d+"active")||(b.direction=n>b.currentSlide?"next":"prev",b.flexAnimate(n,b.vars.pauseOnAction))}// setup flags to prevent event duplication
""===l&&(l=t.type),f.setToClearWatchedEvent()})},setupManual:function t(){b.controlNav=b.manualControls,f.controlNav.active(),b.controlNav.bind(c,function(t){if(t.preventDefault(),""===l||l===t.type){var e=h(this),n=b.controlNav.index(e);e.hasClass(d+"active")||(n>b.currentSlide?b.direction="next":b.direction="prev",b.flexAnimate(n,b.vars.pauseOnAction))}// setup flags to prevent event duplication
""===l&&(l=t.type),f.setToClearWatchedEvent()})},set:function t(){var e="thumbnails"===b.vars.controlNav?"img":"a";b.controlNav=h("."+d+"control-nav li "+e,b.controlsContainer?b.controlsContainer:b)},active:function t(){b.controlNav.removeClass(d+"active").eq(b.animatingTo).addClass(d+"active")},update:function t(e,n){1<b.pagingCount&&"add"===e?b.controlNavScaffold.append(h('<li><a href="#">'+b.count+"</a></li>")):1===b.pagingCount?b.controlNavScaffold.find("li").remove():b.controlNav.eq(n).closest("li").remove(),f.controlNav.set(),1<b.pagingCount&&b.pagingCount!==b.controlNav.length?b.update(n,e):f.controlNav.active()}},directionNav:{setup:function t(){var e=h('<ul class="'+d+'direction-nav"><li class="'+d+'nav-prev"><a class="'+d+'prev" href="#">'+b.vars.prevText+'</a></li><li class="'+d+'nav-next"><a class="'+d+'next" href="#">'+b.vars.nextText+"</a></li></ul>");// CUSTOM DIRECTION NAV:
b.customDirectionNav?b.directionNav=b.customDirectionNav:b.controlsContainer?(h(b.controlsContainer).append(e),b.directionNav=h("."+d+"direction-nav li a",b.controlsContainer)):(b.append(e),b.directionNav=h("."+d+"direction-nav li a",b)),f.directionNav.update(),b.directionNav.bind(c,function(t){var e;t.preventDefault(),""!==l&&l!==t.type||(e=h(this).hasClass(d+"next")?b.getTarget("next"):b.getTarget("prev"),b.flexAnimate(e,b.vars.pauseOnAction)),// setup flags to prevent event duplication
""===l&&(l=t.type),f.setToClearWatchedEvent()})},update:function t(){var e=d+"disabled";1===b.pagingCount?b.directionNav.addClass(e).attr("tabindex","-1"):b.vars.animationLoop?b.directionNav.removeClass(e).removeAttr("tabindex"):0===b.animatingTo?b.directionNav.removeClass(e).filter("."+d+"prev").addClass(e).attr("tabindex","-1"):b.animatingTo===b.last?b.directionNav.removeClass(e).filter("."+d+"next").addClass(e).attr("tabindex","-1"):b.directionNav.removeClass(e).removeAttr("tabindex")}},pausePlay:{setup:function t(){var e=h('<div class="'+d+'pauseplay"><a href="#"></a></div>');// CONTROLSCONTAINER:
b.controlsContainer?(b.controlsContainer.append(e),b.pausePlay=h("."+d+"pauseplay a",b.controlsContainer)):(b.append(e),b.pausePlay=h("."+d+"pauseplay a",b)),f.pausePlay.update(b.vars.slideshow?d+"pause":d+"play"),b.pausePlay.bind(c,function(t){t.preventDefault(),""!==l&&l!==t.type||(h(this).hasClass(d+"pause")?(b.manualPause=!0,b.manualPlay=!1,b.pause()):(b.manualPause=!1,b.manualPlay=!0,b.play())),// setup flags to prevent event duplication
""===l&&(l=t.type),f.setToClearWatchedEvent()})},update:function t(e){"play"===e?b.pausePlay.removeClass(d+"pause").addClass(d+"play").html(b.vars.playText):b.pausePlay.removeClass(d+"play").addClass(d+"pause").html(b.vars.pauseText)}},touch:function t(){var i,s,r,c,l,d,e,o,u,p=!1,a=0,f=0,h=0;if(y){var n=function t(e){e.stopPropagation(),b.animating?e.preventDefault():(b.pause(),m._gesture.addPointer(e.pointerId),h=0,c=x?b.h:b.w,d=Number(new Date),// CAROUSEL:
r=S&&w&&b.animatingTo===b.last?0:S&&w?b.limit-(b.itemW+b.vars.itemMargin)*b.move*b.animatingTo:S&&b.currentSlide===b.last?b.limit:S?(b.itemW+b.vars.itemMargin)*b.move*b.currentSlide:w?(b.last-b.currentSlide+b.cloneOffset)*c:(b.currentSlide+b.cloneOffset)*c)},g=function t(e){e.stopPropagation();var n=e.target._slider;if(n){var a=-e.translationX,o=-e.translationY;//Accumulate translations.
h+=x?o:a,l=(n.vars.rtl?-1:1)*h,p=x?Math.abs(h)<Math.abs(-a):Math.abs(h)<Math.abs(-o),e.detail!==e.MSGESTURE_FLAG_INERTIA?(!p||500<Number(new Date)-d)&&(e.preventDefault(),!C&&n.transitions&&(n.vars.animationLoop||(l=h/(0===n.currentSlide&&h<0||n.currentSlide===n.last&&0<h?Math.abs(h)/c+2:1)),n.setProps(r+l,"setTouch"))):setImmediate(function(){m._gesture.stop()})}},v=function t(e){e.stopPropagation();var n=e.target._slider;if(n){if(n.animatingTo===n.currentSlide&&!p&&null!==l){var a=w?-l:l,o=0<a?n.getTarget("next"):n.getTarget("prev");n.canAdvance(o)&&(Number(new Date)-d<550&&50<Math.abs(a)||Math.abs(a)>c/2)?n.flexAnimate(o,n.vars.pauseOnAction):C||n.flexAnimate(n.currentSlide,n.vars.pauseOnAction,!0)}r=l=s=i=null,h=0}};m.style.msTouchAction="none",m._gesture=new MSGesture,(m._gesture.target=m).addEventListener("MSPointerDown",n,!1),m._slider=b,m.addEventListener("MSGestureChange",g,!1),m.addEventListener("MSGestureEnd",v,!1)}else e=function t(e){b.animating?e.preventDefault():(window.navigator.msPointerEnabled||1===e.touches.length)&&(b.pause(),// CAROUSEL:
c=x?b.h:b.w,d=Number(new Date),// CAROUSEL:
// Local vars for X and Y points.
a=e.touches[0].pageX,f=e.touches[0].pageY,r=S&&w&&b.animatingTo===b.last?0:S&&w?b.limit-(b.itemW+b.vars.itemMargin)*b.move*b.animatingTo:S&&b.currentSlide===b.last?b.limit:S?(b.itemW+b.vars.itemMargin)*b.move*b.currentSlide:w?(b.last-b.currentSlide+b.cloneOffset)*c:(b.currentSlide+b.cloneOffset)*c,i=x?f:a,s=x?a:f,m.addEventListener("touchmove",o,!1),m.addEventListener("touchend",u,!1))},o=function t(e){
// Local vars for X and Y points.
a=e.touches[0].pageX,f=e.touches[0].pageY,l=x?i-f:(b.vars.rtl?-1:1)*(i-a);var n=500;(!(p=x?Math.abs(l)<Math.abs(a-s):Math.abs(l)<Math.abs(f-s))||Number(new Date)-d>n)&&(e.preventDefault(),!C&&b.transitions&&(b.vars.animationLoop||(l/=0===b.currentSlide&&l<0||b.currentSlide===b.last&&0<l?Math.abs(l)/c+2:1),b.setProps(r+l,"setTouch")))},u=function t(e){if(
// finish the touch by undoing the touch session
m.removeEventListener("touchmove",o,!1),b.animatingTo===b.currentSlide&&!p&&null!==l){var n=w?-l:l,a=0<n?b.getTarget("next"):b.getTarget("prev");b.canAdvance(a)&&(Number(new Date)-d<550&&50<Math.abs(n)||Math.abs(n)>c/2)?b.flexAnimate(a,b.vars.pauseOnAction):C||b.flexAnimate(b.currentSlide,b.vars.pauseOnAction,!0)}m.removeEventListener("touchend",u,!1),r=l=s=i=null},m.addEventListener("touchstart",e,!1)},resize:function t(){!b.animating&&b.is(":visible")&&(S||b.doMath(),C?
// SMOOTH HEIGHT:
f.smoothHeight():S?(
//CAROUSEL:
b.slides.width(b.computedW),b.update(b.pagingCount),b.setProps()):x?(
//VERTICAL:
b.viewport.height(b.h),b.setProps(b.h,"setTotal")):(
// SMOOTH HEIGHT:
b.vars.smoothHeight&&f.smoothHeight(),b.newSlides.width(b.computedW),b.setProps(b.computedW,"setTotal")))},smoothHeight:function t(e){if(!x||C){var n=C?b:b.viewport;e?n.animate({height:b.slides.eq(b.animatingTo).innerHeight()},e):n.innerHeight(b.slides.eq(b.animatingTo).innerHeight())}},sync:function t(e){var n=h(b.vars.sync).data("flexslider"),a=b.animatingTo;switch(e){case"animate":n.flexAnimate(a,b.vars.pauseOnAction,!1,!0);break;case"play":n.playing||n.asNav||n.play();break;case"pause":n.pause();break}},uniqueID:function t(e){
// Append _clone to current level and children elements with id attributes
return e.filter("[id]").add(e.find("[id]")).each(function(){var t=h(this);t.attr("id",t.attr("id")+"_clone")}),e},pauseInvisible:{visProp:null,init:function t(){var e=f.pauseInvisible.getHiddenProp();if(e){var n=e.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(n,function(){f.pauseInvisible.isHidden()?b.startTimeout?clearTimeout(b.startTimeout):b.pause():b.started?b.play():0<b.vars.initDelay?setTimeout(b.play,b.vars.initDelay):b.play()})}},isHidden:function t(){var e=f.pauseInvisible.getHiddenProp();return!!e&&document[e]},getHiddenProp:function t(){var e=["webkit","moz","ms","o"];// if 'hidden' is natively supported just return it
if("hidden"in document)return"hidden";// otherwise loop over all the known prefixes until we find one
for(var n=0;n<e.length;n++)if(e[n]+"Hidden"in document)return e[n]+"Hidden";// otherwise it's not supported
return null}},setToClearWatchedEvent:function t(){clearTimeout(e),e=setTimeout(function(){l=""},3e3)}},// public methods
b.flexAnimate=function(t,e,n,a,o){if(b.vars.animationLoop||t===b.currentSlide||(b.direction=t>b.currentSlide?"next":"prev"),p&&1===b.pagingCount&&(b.direction=b.currentItem<t?"next":"prev"),!b.animating&&(b.canAdvance(t,o)||n)&&b.is(":visible")){if(p&&a){var i=h(b.vars.asNavFor).data("flexslider");if(b.atEnd=0===t||t===b.count-1,i.flexAnimate(t,!0,!1,!0,o),b.direction=b.currentItem<t?"next":"prev",i.direction=b.direction,Math.ceil((t+1)/b.visible)-1===b.currentSlide||0===t)return b.currentItem=t,b.slides.removeClass(d+"active-slide").eq(t).addClass(d+"active-slide"),!1;b.currentItem=t,b.slides.removeClass(d+"active-slide").eq(t).addClass(d+"active-slide"),t=Math.floor(t/b.visible)}// SLIDE:
if(b.animating=!0,b.animatingTo=t,// SLIDESHOW:
e&&b.pause(),// API: before() animation Callback
b.vars.before(b),// SYNC:
b.syncExists&&!o&&f.sync("animate"),// CONTROLNAV
b.vars.controlNav&&f.controlNav.active(),// !CAROUSEL:
// CANDIDATE: slide active class (for add/remove slide)
S||b.slides.removeClass(d+"active-slide").eq(t).addClass(d+"active-slide"),// INFINITE LOOP:
// CANDIDATE: atEnd
b.atEnd=0===t||t===b.last,// DIRECTIONNAV:
b.vars.directionNav&&f.directionNav.update(),t===b.last&&(
// API: end() of cycle Callback
b.vars.end(b),// SLIDESHOW && !INFINITE LOOP:
b.vars.animationLoop||b.pause()),C)
// FADE:
u?(b.slides.eq(b.currentSlide).css({opacity:0,zIndex:1}),b.slides.eq(t).css({opacity:1,zIndex:2}),b.wrapup(s)):(b.slides.eq(b.currentSlide).css({zIndex:1}).animate({opacity:0},b.vars.animationSpeed,b.vars.easing),b.slides.eq(t).css({zIndex:2}).animate({opacity:1},b.vars.animationSpeed,b.vars.easing,b.wrapup));else{var s=x?b.slides.filter(":first").height():b.computedW,r,c,l;// INFINITE LOOP / REVERSE:
c=S?(r=b.vars.itemMargin,(l=(b.itemW+r)*b.move*b.animatingTo)>b.limit&&1!==b.visible?b.limit:l):0===b.currentSlide&&t===b.count-1&&b.vars.animationLoop&&"next"!==b.direction?w?(b.count+b.cloneOffset)*s:0:b.currentSlide===b.last&&0===t&&b.vars.animationLoop&&"prev"!==b.direction?w?0:(b.count+1)*s:w?(b.count-1-t+b.cloneOffset)*s:(t+b.cloneOffset)*s,b.setProps(c,"",b.vars.animationSpeed),b.transitions?(b.vars.animationLoop&&b.atEnd||(b.animating=!1,b.currentSlide=b.animatingTo),// Unbind previous transitionEnd events and re-bind new transitionEnd event
b.container.unbind("webkitTransitionEnd transitionend"),b.container.bind("webkitTransitionEnd transitionend",function(){clearTimeout(b.ensureAnimationEnd),b.wrapup(s)}),// Insurance for the ever-so-fickle transitionEnd event
clearTimeout(b.ensureAnimationEnd),b.ensureAnimationEnd=setTimeout(function(){b.wrapup(s)},b.vars.animationSpeed+100)):b.container.animate(b.args,b.vars.animationSpeed,b.vars.easing,function(){b.wrapup(s)})}// SMOOTH HEIGHT:
b.vars.smoothHeight&&f.smoothHeight(b.vars.animationSpeed)}},b.wrapup=function(t){
// SLIDE:
C||S||(0===b.currentSlide&&b.animatingTo===b.last&&b.vars.animationLoop?b.setProps(t,"jumpEnd"):b.currentSlide===b.last&&0===b.animatingTo&&b.vars.animationLoop&&b.setProps(t,"jumpStart")),b.animating=!1,b.currentSlide=b.animatingTo,// API: after() animation Callback
b.vars.after(b)},// SLIDESHOW:
b.animateSlides=function(){!b.animating&&n&&b.flexAnimate(b.getTarget("next"))},// SLIDESHOW:
b.pause=function(){clearInterval(b.animatedSlides),b.animatedSlides=null,b.playing=!1,// PAUSEPLAY:
b.vars.pausePlay&&f.pausePlay.update("play"),// SYNC:
b.syncExists&&f.sync("pause")},// SLIDESHOW:
b.play=function(){b.playing&&clearInterval(b.animatedSlides),b.animatedSlides=b.animatedSlides||setInterval(b.animateSlides,b.vars.slideshowSpeed),b.started=b.playing=!0,// PAUSEPLAY:
b.vars.pausePlay&&f.pausePlay.update("pause"),// SYNC:
b.syncExists&&f.sync("play")},// STOP:
b.stop=function(){b.pause(),b.stopped=!0},b.canAdvance=function(t,e){
// ASNAV:
var n=p?b.pagingCount-1:b.last;return!!e||(!(!p||b.currentItem!==b.count-1||0!==t||"prev"!==b.direction)||(!p||0!==b.currentItem||t!==b.pagingCount-1||"next"===b.direction)&&(!(t===b.currentSlide&&!p)&&(!!b.vars.animationLoop||(!b.atEnd||0!==b.currentSlide||t!==n||"next"===b.direction)&&(!b.atEnd||b.currentSlide!==n||0!==t||"next"!==b.direction))))},b.getTarget=function(t){return"next"===(b.direction=t)?b.currentSlide===b.last?0:b.currentSlide+1:0===b.currentSlide?b.last:b.currentSlide-1},// SLIDE:
b.setProps=function(t,e,n){var a=(o=t||(b.itemW+b.vars.itemMargin)*b.move*b.animatingTo,function(){if(S)return"setTouch"===e?t:w&&b.animatingTo===b.last?0:w?b.limit-(b.itemW+b.vars.itemMargin)*b.move*b.animatingTo:b.animatingTo===b.last?b.limit:o;switch(e){case"setTotal":return w?(b.count-1-b.currentSlide+b.cloneOffset)*t:(b.currentSlide+b.cloneOffset)*t;case"setTouch":return t;case"jumpEnd":return w?t:b.count*t;case"jumpStart":return w?b.count*t:t;default:return t}}()*(b.vars.rtl?1:-1)+"px"),o,i;b.transitions&&(a=x?"translate3d(0,"+a+",0)":"translate3d("+(b.vars.rtl?-1:1)*parseInt(a)+"px,0,0)",n=void 0!==n?n/1e3+"s":"0s",b.container.css("-"+b.pfx+"-transition-duration",n),b.container.css("transition-duration",n)),b.args[b.prop]=a,(b.transitions||void 0===n)&&b.container.css(b.args),b.container.css("transform",a)},b.setup=function(t){var e,n;
// SLIDE:
C?(
// FADE:
b.vars.rtl?b.slides.css({width:"100%",float:"right",marginLeft:"-100%",position:"relative"}):b.slides.css({width:"100%",float:"left",marginRight:"-100%",position:"relative"}),"init"===t&&(u?b.slides.css({opacity:0,display:"block",webkitTransition:"opacity "+b.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(b.currentSlide).css({opacity:1,zIndex:2}):
//slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
0==b.vars.fadeFirstSlide?b.slides.css({opacity:0,display:"block",zIndex:1}).eq(b.currentSlide).css({zIndex:2}).css({opacity:1}):b.slides.css({opacity:0,display:"block",zIndex:1}).eq(b.currentSlide).css({zIndex:2}).animate({opacity:1},b.vars.animationSpeed,b.vars.easing)),// SMOOTH HEIGHT:
b.vars.smoothHeight&&f.smoothHeight()):("init"===t&&(b.viewport=h('<div class="'+d+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(b).append(b.container),// INFINITE LOOP:
b.cloneCount=0,b.cloneOffset=0,// REVERSE:
w&&(n=h.makeArray(b.slides).reverse(),b.slides=h(n),b.container.empty().append(b.slides))),// INFINITE LOOP && !CAROUSEL:
b.vars.animationLoop&&!S&&(b.cloneCount=2,b.cloneOffset=1,// clear out old clones
"init"!==t&&b.container.find(".clone").remove(),b.container.append(f.uniqueID(b.slides.first().clone().addClass("clone")).attr("aria-hidden","true")).prepend(f.uniqueID(b.slides.last().clone().addClass("clone")).attr("aria-hidden","true"))),b.newSlides=h(b.vars.selector,b),e=w?b.count-1-b.currentSlide+b.cloneOffset:b.currentSlide+b.cloneOffset,// VERTICAL:
x&&!S?(b.container.height(200*(b.count+b.cloneCount)+"%").css("position","absolute").width("100%"),setTimeout(function(){b.newSlides.css({display:"block"}),b.doMath(),b.viewport.height(b.h),b.setProps(e*b.h,"init")},"init"===t?100:0)):(b.container.width(200*(b.count+b.cloneCount)+"%"),b.setProps(e*b.computedW,"init"),setTimeout(function(){b.doMath(),b.vars.rtl,b.newSlides.css({width:b.computedW,marginRight:b.computedM,float:"left",display:"block"}),// SMOOTH HEIGHT:
b.vars.smoothHeight&&f.smoothHeight()},"init"===t?100:0)));// !CAROUSEL:
// CANDIDATE: active slide
S||b.slides.removeClass(d+"active-slide").eq(b.currentSlide).addClass(d+"active-slide"),//FlexSlider: init() Callback
b.vars.init(b)},b.doMath=function(){var t=b.slides.first(),e=b.vars.itemMargin,n=b.vars.minItems,a=b.vars.maxItems;b.w=void 0===b.viewport?b.width():b.viewport.width(),b.h=t.height(),b.boxPadding=t.outerWidth()-t.width(),// CAROUSEL:
S?(b.itemT=b.vars.itemWidth+e,b.itemM=e,b.minW=n?n*b.itemT:b.w,b.maxW=a?a*b.itemT-e:b.w,b.itemW=b.minW>b.w?(b.w-e*(n-1))/n:b.maxW<b.w?(b.w-e*(a-1))/a:b.vars.itemWidth>b.w?b.w:b.vars.itemWidth,b.visible=Math.floor(b.w/b.itemW),b.move=0<b.vars.move&&b.vars.move<b.visible?b.vars.move:b.visible,b.pagingCount=Math.ceil((b.count-b.visible)/b.move+1),b.last=b.pagingCount-1,b.limit=1===b.pagingCount?0:b.vars.itemWidth>b.w?b.itemW*(b.count-1)+e*(b.count-1):(b.itemW+e)*b.count-b.w-e):(b.itemW=b.w,b.itemM=e,b.pagingCount=b.count,b.last=b.count-1),b.computedW=b.itemW-b.boxPadding,b.computedM=b.itemM},b.update=function(t,e){b.doMath(),// update currentSlide and slider.animatingTo if necessary
S||(t<b.currentSlide?b.currentSlide+=1:t<=b.currentSlide&&0!==t&&(b.currentSlide-=1),b.animatingTo=b.currentSlide),// update controlNav
b.vars.controlNav&&!b.manualControls&&("add"===e&&!S||b.pagingCount>b.controlNav.length?f.controlNav.update("add"):("remove"===e&&!S||b.pagingCount<b.controlNav.length)&&(S&&b.currentSlide>b.last&&(b.currentSlide-=1,b.animatingTo-=1),f.controlNav.update("remove",b.last))),// update directionNav
b.vars.directionNav&&f.directionNav.update()},b.addSlide=function(t,e){var n=h(t);b.count+=1,b.last=b.count-1,// append new slide
x&&w?void 0!==e?b.slides.eq(b.count-e).after(n):b.container.prepend(n):void 0!==e?b.slides.eq(e).before(n):b.container.append(n),// update currentSlide, animatingTo, controlNav, and directionNav
b.update(e,"add"),// update slider.slides
b.slides=h(b.vars.selector+":not(.clone)",b),// re-setup the slider to accomdate new slide
b.setup(),//FlexSlider: added() Callback
b.vars.added(b)},b.removeSlide=function(t){var e=isNaN(t)?b.slides.index(h(t)):t;// update count
b.count-=1,b.last=b.count-1,// remove slide
isNaN(t)?h(t,b.slides).remove():x&&w?b.slides.eq(b.last).remove():b.slides.eq(t).remove(),// update currentSlide, animatingTo, controlNav, and directionNav
b.doMath(),b.update(e,"remove"),// update slider.slides
b.slides=h(b.vars.selector+":not(.clone)",b),// re-setup the slider to accomdate new slide
b.setup(),// FlexSlider: removed() Callback
b.vars.removed(b)},//FlexSlider: Initialize
f.init()},// Ensure the slider isn't focussed if the window loses focus.
h(window).blur(function(t){n=!1}).focus(function(t){n=!0}),//FlexSlider: Default Settings
h.flexslider.defaults={namespace:"flex-",
//{NEW} String: Prefix string attached to the class of every element generated by the plugin
selector:".slides > li",
//{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
animation:"fade",
//String: Select your animation type, "fade" or "slide"
easing:"swing",
//{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
direction:"horizontal",
//String: Select the sliding direction, "horizontal" or "vertical"
reverse:!1,
//{NEW} Boolean: Reverse the animation direction
animationLoop:!0,
//Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
smoothHeight:!1,
//{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
startAt:0,
//Integer: The slide that the slider should start on. Array notation (0 = first slide)
slideshow:!0,
//Boolean: Animate slider automatically
slideshowSpeed:7e3,
//Integer: Set the speed of the slideshow cycling, in milliseconds
animationSpeed:600,
//Integer: Set the speed of animations, in milliseconds
initDelay:0,
//{NEW} Integer: Set an initialization delay, in milliseconds
randomize:!1,
//Boolean: Randomize slide order
fadeFirstSlide:!0,
//Boolean: Fade in the first slide when animation type is "fade"
thumbCaptions:!1,
//Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.
// Usability features
pauseOnAction:!0,
//Boolean: Pause the slideshow when interacting with control elements, highly recommended.
pauseOnHover:!1,
//Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
pauseInvisible:!0,
//{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
useCSS:!0,
//{NEW} Boolean: Slider will use CSS3 transitions if available
touch:!0,
//{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
video:!1,
//{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches
// Primary Controls
controlNav:!0,
//Boolean: Create navigation for paging control of each slide? Note: Leave true for manualControls usage
directionNav:!0,
//Boolean: Create navigation for previous/next navigation? (true/false)
prevText:"Previous",
//String: Set the text for the "previous" directionNav item
nextText:"Next",
//String: Set the text for the "next" directionNav item
// Secondary Navigation
keyboard:!0,
//Boolean: Allow slider navigating via keyboard left/right keys
multipleKeyboard:!1,
//{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
mousewheel:!1,
//{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
pausePlay:!1,
//Boolean: Create pause/play dynamic element
pauseText:"Pause",
//String: Set the text for the "pause" pausePlay item
playText:"Play",
//String: Set the text for the "play" pausePlay item
// Special properties
controlsContainer:"",
//{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
manualControls:"",
//{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
customDirectionNav:"",
//{NEW} jQuery Object/Selector: Custom prev / next button. Must be two jQuery elements. In order to make the events work they have to have the classes "prev" and "next" (plus namespace)
sync:"",
//{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
asNavFor:"",
//{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider
// Carousel Options
itemWidth:0,
//{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
itemMargin:0,
//{NEW} Integer: Margin between carousel items.
minItems:1,
//{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
maxItems:0,
//{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
move:0,
//{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
allowOneSlide:!0,
//{NEW} Boolean: Whether or not to allow a slider comprised of a single slide
// Callback API
start:function t(){},
//Callback: function(slider) - Fires when the slider loads the first slide
before:function t(){},
//Callback: function(slider) - Fires asynchronously with each slider animation
after:function t(){},
//Callback: function(slider) - Fires after each slider animation completes
end:function t(){},
//Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
added:function t(){},
//{NEW} Callback: function(slider) - Fires after a slide is added
removed:function t(){},
//{NEW} Callback: function(slider) - Fires after a slide is removed
init:function t(){},
//{NEW} Callback: function(slider) - Fires after the slider is initially setup
rtl:!1},//FlexSlider: Plugin Function
h.fn.flexslider=function(a){if(void 0===a&&(a={}),"object"===_typeof(a))return this.each(function(){var t=h(this),e=a.selector?a.selector:".slides > li",n=t.find(e);1===n.length&&!1===a.allowOneSlide||0===n.length?(n.fadeIn(400),a.start&&a.start(t)):void 0===t.data("flexslider")&&new h.flexslider(this,a)});
// Helper strings to quickly perform functions on the slider
var t=h(this).data("flexslider");switch(a){case"play":t.play();break;case"pause":t.pause();break;case"stop":t.stop();break;case"next":t.flexAnimate(t.getTarget("next"),!0);break;case"prev":case"previous":t.flexAnimate(t.getTarget("prev"),!0);break;default:"number"==typeof a&&t.flexAnimate(a,!0)}}}(jQuery),
/*!
 * jQuery Cookiebar Plugin
 * https://github.com/carlwoodhouse/jquery.cookieBar
 *
 * Copyright 2012-17, Carl Woodhouse. the cookie function is inspired by https://github.com/carhartl/jquery-cookie
 * Disclaimer: if you still get fined for not complying with the eu cookielaw, it's not our fault.
 * Licence: MIT
 */
function(l){l.cookie=function(t,e,n){if(1<arguments.length&&(!/Object/.test(Object.prototype.toString.call(e))||null==e)){if(n=l.extend({},n),null==e&&(n.expires=-1),"number"==typeof n.expires){var a=n.expires,o=n.expires=new Date;o.setDate(o.getDate()+a)}return e=String(e),document.cookie=[encodeURIComponent(t),"=",n.raw?e:encodeURIComponent(e),n.expires?"; expires="+n.expires.toUTCString():"",// max-age is not supported by IE
n.path?"; path="+n.path:"",n.domain?"; domain="+n.domain:"",n.secure?"; secure":""].join("")}for(var i=(n=e||{}).raw?function(t){return t}:decodeURIComponent,s=document.cookie.split("; "),r=0,c;c=s[r]&&s[r].split("=");r++)
// IE
if(i(c[0])===t)return i(c[1]||"");return null},l.fn.cookieBar=function(t){var e=l.extend({closeButton:"none",hideOnClose:!0,secure:!1,path:"/",domain:""},t);return this.each(function(){var t=l(this);// just in case they didnt hide it by default.
t.hide(),// if close button not defined. define it!
"none"==e.closeButton&&(t.append('<a class="cookiebar-close">Continue</a>'),l.extend(e,{closeButton:".cookiebar-close"})),"hide"!=l.cookie("cookiebar")&&t.show(),t.find(e.closeButton).click(function(){return e.hideOnClose&&t.hide(),l.cookie("cookiebar","hide",{path:e.path,secure:e.secure,domain:e.domain,expires:30}),t.trigger("cookieBar-close"),!1})})},// self injection init
l.cookieBar=function(t){l("body").prepend('<div class="ui-widget"><div style="display: none;" class="cookie-message ui-widget-header blue"><p>By using this website you allow us to place cookies on your computer. They are harmless and never personally identify you.</p></div></div>'),l(".cookie-message").cookieBar(t)}}(jQuery),jQuery(document).ready(function(t){var e;
// Theme Main JQuery functions
t(".handle").on("click",function(){t("nav ul.main-nav").toggleClass("show")}),// Adding :hover & :focus states on Mobile
window.onload=function(){/iP(hone|ad)/.test(window.navigator.userAgent)&&document.body.addEventListener("touchstart",function(){},!1)},// jQuery Cookie notice
t("#cookie-notice").cookieBar({closeButton:".cookie-dismiss",hideOnClose:!1}),t("#cookie-notice").on("cookieBar-close",function(){t(this).fadeOut("1s")}),// Initialize the Lightbox and add rel="gallery" to all gallery images when the gallery is set up using [gallery link="file"] so that a Lightbox Gallery exists
t("[data-fancybox]").fancybox({youtube:{showinfo:0}}),t(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").attr("data-fancybox","gallery").fancybox(),t('a[href$="jpg"], a[href$="png"], a[href$="jpeg"]').fancybox((_defineProperty(e={thumbs:!1,fullScreen:!1},"thumbs",!1),_defineProperty(e,"loop",!1),_defineProperty(e,"slideShow",!1),_defineProperty(e,"clickContent",!1),_defineProperty(e,"buttons",["slideShow","fullScreen","thumbs",//'share',
//'download',
//'zoom',
"close"]),e))});
//# sourceMappingURL=all.js.map