<?php
/**
 * @widget Sociale media links from contact info
 */
class rby_social extends WP_Widget {
	public function __construct()  {
		parent::__construct('rby_social', __('Social media links','rby'), 'description='.__('Shows links to specified social network profiles','rby'));
	}
	
	function form($instance) {
		$title = esc_attr($instance['title']);
		$type = esc_attr($instance['type']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">
				<strong><?php _e('Title'); ?></strong><br />
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</label>
		</p>
		<p>
			<label>
				<strong><?php _e('Icon types','rby'); ?></strong><br />
				<select class="widefat" name="<?php echo $this->get_field_name('type'); ?>">
					<option <?php if($type == 'large-icons') echo 'selected="selected"'; ?> value="large-icons"><?php _e('Large icons','rby'); ?></option>
					<option <?php if($type == 'small-icons') echo 'selected="selected"'; ?> value="small-icons"><?php _e('Small icons','rby'); ?></option>
					<option <?php if($type == 'small-icons-text') echo 'selected="selected"'; ?> value="small-icons-text"><?php _e('Small icons with text','rby'); ?></option>
				</select>
			</label>
		</p>
		<p><?php printf(__('Change the contents of this widget on the <a href="%1$s">contact information</a> page.', 'rby'), admin_url('options-general.php?page=rby-information')); ?></p>
	<?php
	}
	
	function update($new_instance,$old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['type'] = $new_instance['type'];
				
		return $instance;
	}
	
	function widget($args,$instance) {		
		$title = apply_filters('widget_title', $instance['title']);
		$type = $instance['type'];
		extract($args);
	?>
		<?php echo $before_widget; ?>
			<?php if ($title) { echo $before_title . $title . $after_title; } ?>

			<ul class="social <?php echo $type; ?> clearfix">
				<?php if($facebook = get_theme_mod( 'rby_company_facebook' ))  { ?>
					<li class="facebook">
						<a rel="external" href="<?php echo $facebook; ?>" title="<?php _e('Like our Facebook page','rby') ?>" target="_blank">
							<i><svg viewBox="0 0 192 384" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <path d="M128,128 L128,89.9 C128,72.7 131.8,64 158.5,64 L192,64 L192,0 L136.1,0 C67.6,0 45,31.4 45,85.3 L45,128 L2.84217094e-14,128 L2.84217094e-14,192 L45,192 L45,384 L128,384 L128,192 L184.4,192 L192,128 L128,128 Z"></path>
							</svg></i><span class="title"><?php _e('Like our Facebook page','rby') ?></span>
						</a>
					</li>
				<?php } if($instagram = get_theme_mod( 'rby_company_instagram' )) { ?>
					<li class="instagram">
						<a rel="external" href="<?php echo $instagram; ?>" title="<?php _e('Follow us on Instagram','rby') ?>" target="_blank">
							<i><svg viewBox="0 0 504 504" xmlns:xlink="http://www.w3.org/1999/xlink">
           						<path d="M251.921,122.715 C323.367,122.715 381.285,180.633 381.285,252.08 C381.285,323.526 323.367,381.444 251.921,381.444 C180.474,381.444 122.556,323.526 122.556,252.08 C122.556,180.633 180.474,122.715 251.921,122.715 Z M251.921,336.053 C298.298,336.053 335.894,298.457 335.894,252.08 C335.894,205.702 298.298,168.106 251.921,168.106 C205.543,168.106 167.947,205.702 167.947,252.08 C167.947,298.457 205.543,336.053 251.921,336.053 Z" id="Combined-Shape"></path>
            					<path d="M416.627,117.604 C416.627,134.3 403.092,147.834 386.396,147.834 C369.701,147.834 356.166,134.3 356.166,117.604 C356.166,100.908 369.701,87.373 386.396,87.373 C403.092,87.373 416.627,100.908 416.627,117.604" id="Fill-5"></path>
            					<path d="M251.921,0.159 C320.338,0.159 328.917,0.449 355.787,1.675 C382.601,2.898 400.914,7.157 416.938,13.385 C433.504,19.822 447.553,28.436 461.559,42.441 C475.564,56.447 484.178,70.496 490.616,87.062 C496.843,103.086 501.102,121.399 502.325,148.213 C503.551,175.083 503.841,183.662 503.841,252.08 C503.841,320.497 503.551,329.076 502.325,355.946 C501.102,382.76 496.843,401.073 490.616,417.097 C484.178,433.663 475.564,447.712 461.559,461.718 C447.553,475.723 433.504,484.337 416.938,490.775 C400.914,497.002 382.601,501.261 355.787,502.484 C328.917,503.71 320.338,504 251.921,504 C183.503,504 174.924,503.71 148.054,502.484 C121.24,501.261 102.927,497.002 86.903,490.775 C70.337,484.337 56.288,475.723 42.282,461.718 C28.277,447.712 19.663,433.663 13.226,417.097 C6.998,401.073 2.739,382.76 1.516,355.946 C0.29,329.076 0,320.497 0,252.08 C0,183.662 0.29,175.083 1.516,148.213 C2.739,121.399 6.998,103.086 13.226,87.062 C19.663,70.496 28.277,56.447 42.282,42.441 C56.288,28.436 70.337,19.822 86.903,13.385 C102.927,7.157 121.24,2.898 148.054,1.675 C174.924,0.449 183.503,0.159 251.921,0.159 Z M251.921,45.55 C184.655,45.55 176.687,45.807 150.123,47.019 C125.561,48.139 112.222,52.243 103.345,55.693 C91.586,60.263 83.194,65.722 74.379,74.538 C65.563,83.353 60.104,91.745 55.534,103.504 C52.084,112.381 47.98,125.72 46.86,150.282 C45.648,176.846 45.391,184.814 45.391,252.08 C45.391,319.345 45.648,327.313 46.86,353.877 C47.98,378.439 52.084,391.778 55.534,400.655 C60.104,412.414 65.564,420.806 74.379,429.621 C83.194,438.437 91.586,443.896 103.345,448.466 C112.222,451.916 125.561,456.02 150.123,457.14 C176.684,458.352 184.65,458.609 251.921,458.609 C319.191,458.609 327.158,458.352 353.718,457.14 C378.28,456.02 391.619,451.916 400.496,448.466 C412.255,443.896 420.647,438.437 429.462,429.621 C438.278,420.806 443.737,412.414 448.307,400.655 C451.757,391.778 455.861,378.439 456.981,353.877 C458.193,327.313 458.45,319.345 458.45,252.08 C458.45,184.814 458.193,176.846 456.981,150.282 C455.861,125.72 451.757,112.381 448.307,103.504 C443.737,91.745 438.278,83.353 429.462,74.538 C420.647,65.722 412.255,60.263 400.496,55.693 C391.619,52.243 378.28,48.139 353.718,47.019 C327.154,45.807 319.186,45.55 251.921,45.55 Z" id="Combined-Shape"></path>
							</svg></i><span class="title"><?php _e('Follow us on Instagram','rby') ?></span>
						</a>
					</li>
				<?php } if($linkedin = get_theme_mod( 'rby_company_linkedin' )) { ?>
					<li class="linkedin">
						<a rel="external" href="<?php echo $linkedin; ?>" title="<?php _e('Connect with us on LinkedIn','rby') ?>" target="_blank">
							<i><svg viewBox="0 0 384 384" xmlns:xlink="http://www.w3.org/1999/xlink">
            					<path d="M353.2,0 L32.8,0 C15.3,0 0,12.6 0,29.9 L0,351 C0,368.4 15.3,383.9 32.8,383.9 L353.1,383.9 C370.7,383.9 383.9,368.3 383.9,351 L383.9,29.9 C384,12.6 370.7,0 353.2,0 Z M119,320 L64,320 L64,149 L119,149 L119,320 Z M93.4,123 L93,123 C75.4,123 64,109.9 64,93.5 C64,76.8 75.7,64 93.7,64 C111.7,64 122.7,76.7 123.1,93.5 C123.1,109.9 111.7,123 93.4,123 Z M320,320 L265,320 L265,226.5 C265,204.1 257,188.8 237.1,188.8 C221.9,188.8 212.9,199.1 208.9,209.1 C207.4,212.7 207,217.6 207,222.6 L207,320 L152,320 L152,149 L207,149 L207,172.8 C215,161.4 227.5,145 256.6,145 C292.7,145 320,168.8 320,220.1 L320,320 L320,320 Z" id="Shape"></path>
							</svg></i><span class="title"><?php _e('Connect with us on LinkedIn','rby') ?></span>
						</a>
					</li>

				<?php } if($twitter = get_theme_mod( 'rby_company_twitter' ))  { ?>
					<li class="twitter">
						<a rel="external" href="<?php echo $twitter; ?>" title="<?php _e('Follow us on Twitter','rby') ?>" target="_blank">
							<i><svg viewBox="0 0 472 384"  xmlns:xlink="http://www.w3.org/1999/xlink">
							    <path d="M472,45.5 C454.6,53.2 436,58.4 416.4,60.8 C436.4,48.8 451.8,29.8 459,7.2 C440.3,18.3 419.6,26.4 397.5,30.7 C379.8,11.8 354.6,0 326.8,0 C273.3,0 230,43.4 230,96.9 C230,104.5 230.8,111.9 232.5,119 C152,115 80.6,76.4 32.9,17.7 C24.6,32 19.8,48.7 19.8,66.4 C19.8,100 37,129.7 63,147.1 C47,146.7 32,142.3 19,135 L19,136.2 C19,183.2 52.4,222.3 96.7,231.2 C88.6,233.4 80,234.6 71.2,234.6 C65,234.6 58.9,234 53,232.8 C65.3,271.3 101.1,299.3 143.5,300.1 C110.4,326.1 68.6,341.6 23.2,341.6 C15.4,341.6 7.7,341.1 0.1,340.2 C42.8,368 93.7,384 148.3,384 C326.6,384 424,236.3 424,108.2 C424,104 423.9,99.8 423.7,95.7 C442.6,82 459,65 472,45.5 Z"></path>
							</svg></i><span class="title"><?php _e('Follow us on Twitter','rby') ?></span>
						</a>
					</li>
				<?php } if($youtube = get_theme_mod( 'rby_company_youtube' )) { ?>
					<li class="youtube">
						<a rel="external" href="<?php echo $youtube; ?>" title="<?php _e('View our YouTube channel','rby') ?>" target="_blank">
							<i><svg viewBox="0 0 512 384" xmlns:xlink="http://www.w3.org/1999/xlink">
           						<path d="M509.6,84.8 C509.6,39.8 476.5,3.6 435.6,3.6 C380.2,1 323.7,-2.84217094e-14 266,-2.84217094e-14 L257,-2.84217094e-14 L248,-2.84217094e-14 C190.4,-2.84217094e-14 133.8,1 78.4,3.6 C37.6,3.6 4.5,40 4.5,85 C2,120.6 0.9,156.2 1,191.8 C0.9,227.4 2,263 4.4,298.7 C4.4,343.7 37.5,380.2 78.3,380.2 C136.5,382.9 196.2,384.1 256.9,384 C317.7,384.2 377.2,383 435.5,380.2 C476.4,380.2 509.5,343.7 509.5,298.7 C511.9,263 513,227.4 512.9,191.7 C513.1,156.1 512,120.5 509.6,84.8 Z M208,289.9 L208,93.4 L353,191.6 L208,289.9 Z" id="Shape"></path>
							</svg></i><span class="title"><?php _e('View our YouTube channel','rby') ?></span>
						</a>
					</li>

				<?php } ?>


				<?php if(get_theme_mod( 'rby_checkbox_rss' ) == 'true') { ?>
					<li class="rss">
						<a href="<?php bloginfo('rss2_url'); ?>" target="_blank" title="<?php _e('Subscribe via RSS','rby') ?>">
							<i><svg viewBox="0 0 384 384" xmlns:xlink="http://www.w3.org/1999/xlink">
								<path d="M55.9,272.1 C25.1,272.1 0,297.2 0,327.9 C0,358.7 25.1,383.5 55.9,383.5 C86.8,383.5 111.8,358.6 111.8,327.9 C111.8,297.2 86.8,272.1 55.9,272.1 Z" id="Shape"></path>
							    <path d="M0,128 L0,207.9 C48,207.9 94.1,222.1 128,256 C161.9,289.9 176,335.9 176,384 L256,384 C256,244.1 140,128 0,128 Z" id="Shape"></path>
							    <path d="M0,0 L0,79.9 C171,79.9 303.9,212.9 303.9,384 L384,384 C384,172.3 212,0 0,0 Z" id="Shape"></path>
							</svg></i><span class="title"><?php _e('Subscribe to our RSS','rby') ?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		<?php echo $after_widget; ?>
	<?php
	}
}
function rby_social() { 
	register_widget( 'rby_social' ); 
} 
add_action( 'widgets_init', 'rby_social' );
