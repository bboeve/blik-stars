<?php
	
/**
 * @widget Contact info from contact info
 */
class rby_contact extends WP_Widget {
	public function __construct()  {
		parent::__construct('rby_contact', __('Contact information','rby'), 'description='.__('Shows the specified contact information','rby'));
	}

	function form($instance) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'rby' );
	?>
 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>">
 				<strong><?php _e('Title'); ?></strong><br />
 				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
 			</label>
 		</p>
 		<p><?php printf(__('Change the contents of this widget on the <a href="%1$s">contact information</a> page.', 'rby'), admin_url('themes.php?page=rby-information')); ?></p>
	<?php
	}
	
	function update($new_instance,$old_instance) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}
	
	function widget($args,$instance) {		
		$title = apply_filters('widget_title', $instance['title']);
		extract($args);
	?>
 		<?php echo $before_widget; ?>
 			<?php if ($title) { echo $before_title . $title . $after_title; } ?>	
			<div itemscope itemtype="http://schema.org/Organization">
				<p>
					<?php 
						if ($name = get_theme_mod( 'rby_company_name' )) {
						echo '<span itemprop="name"><strong>'.$name.'</strong></span><br />';
					?>
					<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">	
						<?php
							} if ($address = get_theme_mod( 'rby_company_address' )) { 
								echo '<span itemprop="streetAddress">'.$address.'</span><br />'; 
							} if ($postal_code = get_theme_mod( 'rby_company_postal' )) {
								echo '<span itemprop="postalCode">'.$postal_code.'</span>';
							} if ($city = get_theme_mod( 'rby_company_city' )) {
							 echo ' <span itemprop="addressLocality">'.$city.'</span><br />'; 
							} if ($country = get_theme_mod( 'rby_company_country')) {
							 echo '<span itemprop="addressCountry">'.$country.'</span>'; 
							}
						?>
					</span>
				</p>
				<br />
				<p>
					<?php
						if ($email = get_theme_mod( 'rby_company_email' )) { 
							echo'<span class="label">'.__('E-mail','rby').': </span><a itemprop="email" href="mailto:'.$email.'">'.$email.'</a><br />';
						} if ($telephone = get_theme_mod( 'rby_company_phone' )) { 
							echo '<span class="label">'.__('Telephone','rby').': </span><a itemprop="telephone" href="tel:'.$telephone.'">'.$telephone.'</a><br />';
						} if ($mobile = get_theme_mod( 'rby_company_mobile' )) { 
							echo '<span class="label">'.__('Mobile','rby').': </span><a itemprop="mobile" href="tel:'.$mobile.'">'.$mobile.'</a><br />';
						} if ($fax = get_theme_mod( 'rby_company_fax' )) { 
							echo '<span class="label">'.__('Fax','rby').': </span><span itemprop="faxNumber">'.$fax.'</span>';
						} 
					?>
				</p>
				<p>
					<?php
						if ($cc = get_theme_mod( 'rby_company_cc' )) {
							echo '<span class="label">'.__('CC No','rby').': </span>'.$cc.'<br />';
						} if ($vat = get_theme_mod( 'rby_company_vat' )) {
							echo '<span class="label" itemprop="vatID">'.__('VAT No','rby').': </span>'.$vat.'<br />';
						} if ($bankno = get_theme_mod( 'rby_company_bank_no' )){
							if ($bank = !get_theme_mod( 'rby_company_bank' )) {
								$bank = "Bank";
							} else {
								$bank = get_theme_mod( 'rby_company_bank' );
							}
							echo '<span class="label">'.$bank.': </span>'.$bankno;
						} 
					?>
				</p>
			</div>
		<?php echo $after_widget; ?>
	<?php
	}
}
function rby_contact() { 
	register_widget( 'rby_contact' ); 
} 
add_action( 'widgets_init', 'rby_contact' );
