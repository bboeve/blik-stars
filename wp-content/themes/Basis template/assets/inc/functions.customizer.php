<?php 
/**
 * Remove the additional CSS section, introduced in 4.7, from the Customizer.
 * @param $wp_customize WP_Customize_Manager
 */
function rby_remove_sections( $wp_customize ) {
    $wp_customize->remove_section( 'custom_css' );
    $wp_customize->remove_section( 'colors' );
}
add_action( 'customize_register', 'rby_remove_sections', 15 );

function rby_customizer_settings( $wp_customize ) {
  /*
   * Main Company information panel
   */
  $wp_customize->add_panel( 'rby_company_panel', array(
      'priority' => 30,
      'capability' => 'edit_theme_options',
      'title' => __('Contact information','rby'),
      'description' => __('Add all the needed company information in one of the sections below to show it around the website or in one of the widgets.','rby'),
  ) );


  /*
   * Section Company information
   */
  $wp_customize->add_section( 'rby_company' , array(
      'title'      => __('General information','rby'),
      'description' => __('Add the general contact information of your company below.','rby'),
      'priority'   => 10,
      'panel' => 'rby_company_panel',
  ) );

  $wp_customize->add_setting( 'rby_company_name' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );

  $wp_customize->add_control( 'rby_company_name', array(
	  'label' => __('Company name','rby'),
	  'section' => 'rby_company',
	  'settings' => 'rby_company_name',
	  'type' => 'text',
	) );

  $wp_customize->add_setting( 'rby_company_address' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_address', array(
	  'label' => __('Address','rby'),
	  'section' => 'rby_company',
	  'settings' => 'rby_company_address',
	  'type' => 'text',
	) );

  $wp_customize->add_setting( 'rby_company_postal' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_postal', array(
    'label' => __('Postal code','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_postal',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_city' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_city', array(
    'label' => __('City','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_city',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_country' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_country', array(
    'label' => __('Country','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_country',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_email' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_email', array(
    'label' => __('E-mail','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_email',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_phone' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_phone', array(
    'label' => __('Telephone','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_phone',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_mobile' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_mobile', array(
    'label' => __('Mobile','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_mobile',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_fax' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
    $wp_customize->add_control( 'rby_company_fax', array(
    'label' => __('Fax','rby'),
    'section' => 'rby_company',
    'settings' => 'rby_company_fax',
    'type' => 'text',
  ) );


  /*
   * Section Registration numbers & financial
   */
  $wp_customize->add_section( 'rby_company_reg' , array(
      'title'      =>  __( 'Registration numbers & financial', 'rby' ),
      'description' => __('Add the needed registration & financial numbers below to show them on your website.', 'rby' ),
      'priority'   => 20,
      'panel' => 'rby_company_panel',
  ) );

  $wp_customize->add_setting( 'rby_company_cc' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_cc', array(
    'label' => __('CC No.','rby'),
    'section' => 'rby_company_reg',
    'settings' => 'rby_company_cc',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_vat' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_vat', array(
    'label' => __('VAT No.','rby'),
    'section' => 'rby_company_reg',
    'settings' => 'rby_company_vat',
    'type' => 'text',
  ) );

  $wp_customize->add_setting( 'rby_company_bank' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_bank', array(
    'label' => __('Bank name','rby'),
    'section' => 'rby_company_reg',
    'settings' => 'rby_company_bank',
    'type' => 'text',
  ) ); 

  $wp_customize->add_setting( 'rby_company_bank_no' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_bank_no', array(
    'label' => __('Bank No.','rby'),
    'section' => 'rby_company_reg',
    'settings' => 'rby_company_bank_no',
    'type' => 'text',
  ) ); 


  /*
   * Section Social media links
   */
  $wp_customize->add_section( 'rby_company_social' , array(
      'title'      =>  __( 'Social media links', 'rby' ),
      'description' => __('Add all your social media links below to show them on your website', 'rby' ),
      'priority'   => 20,
      'panel' => 'rby_company_panel',
  ) ); 
  
  $wp_customize->add_setting( 'rby_company_facebook' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_facebook', array(
    'label' => __( 'Facebook URL', 'rby' ),
    'section' => 'rby_company_social',
    'settings' => 'rby_company_facebook',
    'type' => 'url',
  ) ); 

  $wp_customize->add_setting( 'rby_company_instagram' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_instagram', array(
    'label' => __( 'Instagram URL', 'rby' ),
    'section' => 'rby_company_social',
    'settings' => 'rby_company_instagram',
    'type' => 'url',
  ) );

  $wp_customize->add_setting( 'rby_company_linkedin' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_linkedin', array(
    'label' => __( 'LinkedIn URL', 'rby' ),
    'section' => 'rby_company_social',
    'settings' => 'rby_company_linkedin',
    'type' => 'url',
  ) );

  $wp_customize->add_setting( 'rby_company_twitter' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_twitter', array(
    'label' => __( 'Twitter URL', 'rby' ),
    'section' => 'rby_company_social',
    'settings' => 'rby_company_twitter',
    'type' => 'url',
  ) );

  $wp_customize->add_setting( 'rby_company_youtube' , array(
      'default'     => '',
      'transport'   => 'refresh',
  ) );
  $wp_customize->add_control( 'rby_company_youtube', array(
    'label' => __( 'YouTube URL', 'rby' ),
    'section' => 'rby_company_social',
    'settings' => 'rby_company_youtube',
    'type' => 'url',
  ) );

  $wp_customize->add_setting( 'rby_checkbox_rss', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'rby_sanitize_checkbox',
  ) );

  $wp_customize->add_control( 'rby_checkbox_rss', array(
    'type' => 'checkbox',
    'section' => 'rby_company_social',
    'label' => __( 'Show RSS feed in the social media widget', 'rby' ),
  ) );

  function rby_sanitize_checkbox( $checked ) {
    // Boolean check.
    return ( ( isset( $checked ) && true == $checked ) ? true : false );
  }
} 
add_action( 'customize_register', 'rby_customizer_settings' );


function rby_customizer_cookie( $wp_customize ) {

  $wp_customize->add_section( 'rby_cookie_notice', array(
      'priority' => 40,
      'capability' => 'edit_theme_options',
      'title' => __( 'Cookie Notice', 'rby' ),
      'description' => __( 'Add and delete your cookie notice.', 'rby' ),
  ) );

    $wp_customize->add_setting( 'rby_cookie_checkbox', array(

    'capability' => 'edit_theme_options',
    'transport' => 'refresh',
    'sanitize_callback' => 'rby_sanitize_cookie_checkbox',

  ) );

  $wp_customize->add_control( 'rby_cookie_checkbox', array(
    'type' => 'checkbox',
    'section' => 'rby_cookie_notice',
    'label' => __( 'Show the cookie notice.', 'rby' ),
  ) );

  function rby_sanitize_cookie_checkbox( $checked ) { 
    // Boolean check.
    return ( ( isset( $checked ) && true == $checked ) ? true : false );
  }

  $wp_customize->add_setting( 'rby_cookie_text', array(
     'default'     => '',
     'transport'   => 'refresh',
  ) );

  $wp_customize->add_control( 'rby_cookie_text', array(
      'type' => 'textarea',
      'section' => 'rby_cookie_notice',
      'label' => __( 'Cookie notice text', 'rby' ),
      'description' => '',
  ) );

  $wp_customize->add_setting( 'rby_policy_url', array(
     'default'     => '',
     'transport'   => 'refresh',
  ) );

  $wp_customize->add_control( 'rby_policy_url', array(
      'type' => 'url',
      'section' => 'rby_cookie_notice',
      'label' => __( 'Link to privacy policy', 'rby' ),
      'description' => '',
  ) );

  $wp_customize->add_setting( 'rby_policy_text', array(
     'default'     => '',
     'transport'   => 'refresh',
  ) );

  $wp_customize->add_control( 'rby_policy_text', array(
      'type' => 'text',
      'section' => 'rby_cookie_notice',
      'label' => __( 'Privacy policy link text', 'rby' ),
      'description' => '',
  ) );

  $wp_customize->add_setting( 'rby_cookie_btn', array(
     'default'     => '',
     'transport'   => 'refresh',
  ) );

  $wp_customize->add_control( 'rby_cookie_btn', array(
      'type' => 'text',
      'section' => 'rby_cookie_notice',
      'label' => __( 'Accept button text', 'rby' ),
      'description' => '',
  ) );
}
add_action( 'customize_register', 'rby_customizer_cookie' );

?>