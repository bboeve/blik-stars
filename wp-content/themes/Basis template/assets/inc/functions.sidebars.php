<?php

/**
 * Register a sidebar
 *
 * @param string $id Unique sidebar ID
 * @param array $args
 */
 
class rbySidebar {
	function __construct($id,$args) {
		if(function_exists('register_sidebar')) {
			$default = array(
				'before_widget' => '<div class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>'
			);
			
			$args['id'] = $id;
			
			register_sidebar(wp_parse_args($args,$default));
		}
	}
}

/**
 * @sidebars Register the sidebars
 */

function rby_register_sidebars() {
	new rbySidebar('blog',array(
		'name' => __('Blog','rby')
	));
	new rbySidebar('footer1',array(
		'name' => __('Footer 1','rby')
	));
	new rbySidebar('footer2',array(
		'name' => __('Footer 2','rby')
	));
	new rbySidebar('footer3',array(
		'name' => __('Footer 3','rby')
	));
	new rbySidebar('footer4',array(
		'name' => __('Footer 4','rby')
	));
}
add_action('init','rby_register_sidebars');
?>