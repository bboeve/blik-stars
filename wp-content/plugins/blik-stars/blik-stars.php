<?php
/**
 * The standard wordpress plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Blik_Stars
 *
 * @wordpress-plugin
 * Plugin Name:       Blik Stars
 * Plugin URI:        https://www.blikvormgeving.nl
 * Description:       A star rating plugin for wordpress
 * Version:           1.0.0
 * Author:            Boris
 * Author URI:        https://borisboeve.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       blik-stars
 * Domain Path:       /languages
 */


 /**
  * call the standard wordpress functions to interact with the database
  */
global $wpdb;
/**
  * add the version number to the plugin database
  */   
define('BLIKSTARSVERSION', '1.0');
/**
  * define the plugin table name in the databse
  */    
define('BLIKSTARSVOTESTBL', $wpdb->prefix . 'blik_stars_votes');

/**
  * registers the the plugin install function 
  */
register_activation_hook(__FILE__, 'blik_stars_install');
/**
  * registers the the plugin install uninstall 
  */
register_deactivation_hook(__FILE__, 'blik_stars_uninstall');
   /**
     *  linking scripts to Wordpress generated page
     */    
add_action('wp_enqueue_scripts', 'blik_stars_include_scripts');
   /**
     *  linking scripts to to the wordpress admin page
     */   
add_action('admin_enqueue_scripts', 'blik_stars_include_scripts');


/** 
 *  Creates database tables when installing the plugin if they not exist
 */
function blik_stars_install() {

    $blik_stars_votes_table = BLIKSTARSVOTESTBL;
    $sql_yasr_votes_table = "CREATE TABLE IF NOT EXISTS $blik_stars_votes_table (
  		id bigint(20) NOT NULL AUTO_INCREMENT,
  		post_id bigint(20) NOT NULL,
 	 	reviewer_id bigint(20) NOT NULL,
 	 	overall_rating decimal(2,1) NOT NULL,
 	 	number_of_votes bigint(20) NOT NULL,
  		sum_votes decimal(11,1) NOT NULL,
  		review_type VARCHAR(10),
 		PRIMARY KEY  (id),
 		UNIQUE KEY post_id (post_id)	
	);";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql_yasr_votes_table);

    //Makesure the posttype registered before flush rewrite
    blik_stars_post_reviewinit();
    blik_stars_rewriterules();
}


   /**
     * function that rewrites / flushes the links when uninstalling
     */    
function blik_stars_uninstall() {
    blik_stars_rewriterules();
}


function blik_stars_include_scripts() {
    /**
     * function to include the open source compressed Jrate plugin
     */
    wp_enqueue_script(
            'blik-stars-jqrate', plugins_url('/asset/jRate.min.js', __FILE__), array('jquery'), BLIKSTARSVERSION, true
    );
    /**
     * function to include custom script file
     */    
    wp_enqueue_script(
            'blik-stars-jqmain', plugins_url('/asset/main.js', __FILE__), array('jquery', 'blik-stars-jqrate'), BLIKSTARSVERSION, true
    );    
    /**
     * function to include custom styling css file
     */
    
    wp_enqueue_style( 'blik-stars-style', plugins_url('/asset/style.css', __FILE__), array(), BLIKSTARSVERSION );
}

/**
 *  function for flushing/rewrite the wordpress urls to add the plugin custom post type page
 */

function blik_stars_rewriterules() {
    flush_rewrite_rules();
}

/**
 * require the blik stars star rating php file
 */
require ( dirname(__FILE__) . '/blik-stars-post.php' );
/**
 * require the blik stars settings page php file
 */
require ( dirname(__FILE__) . '/blik-stars-settings.php' );
