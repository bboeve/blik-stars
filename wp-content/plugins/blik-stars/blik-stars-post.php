<?php
/**
 * Initialize the post-type 'review' and metabox to save rating informations.
 */
add_action('init', 'blik_stars_post_reviewinit');
add_action('add_meta_boxes', 'blik_stars_meta_addinputbox');
add_action('save_post', 'blik_stars_meta_save_ratinginfo');
add_filter('the_content', 'blik_stars_add_schema');
add_shortcode('blik-stars-add-rating', 'blik_stars_add_rating_shortcode');
add_shortcode('blik-stars-form', 'blik_stars_add_rating_shortcode');
add_shortcode('blik-stars-overall', 'blik_stars_get_overall_rating_shortcode');


/**
 * function to add the custom post type page in the admin menu with options
 */

function blik_stars_post_reviewinit() {
    $args = array(
        'public' => true,
        'label' => 'Blik stars',
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'query_var' => 'reviews',
        'rewrite' => array('slug' => 'reviews'),
        'menu_icon' => 'dashicons-star-empty'
    );
    register_post_type('reviews', $args);
}

function blik_stars_meta_addinputbox() {
    $screens = array('reviews');
    foreach ($screens as $screen) {
        /*         * add_meta_box(
          'starrating_sectionid', __('Review Info', 'starrating_textdomain'), 'blik_stars_meta_reviewedby', $screen
          );* */
          /**
           * register 
           */
        add_meta_box(
                'starrating_stars', __('Blik Stars', 'starrating_textdomain'), 'blik_stars_meta_starrating', $screen, 'side', 'high'
        );
    }
}


function blik_stars_meta_starrating($post) {
    global $wpdb;

    wp_nonce_field('starrating_meta_box', 'starrating_meta_box_nonce');
    $post_id = $post->ID;
    $result = $wpdb->get_row("SELECT * FROM " . BLIKSTARSVOTESTBL . " WHERE post_id=$post_id");
/**
 * The overall rating
 */
    $overall_rating = $result->overall_rating;



    $overall_rating = (empty($overall_rating)) ? 0 : $overall_rating;
/**
 * The overall rating HTML markup
 */

    echo '<label for="customer_review">';
    _e('Huidige rating ', 'starrating_textdomain');
    echo '<span id="customer_rate">' . $overall_rating . '</span></label>';
    echo '<input type="hidden" name="customer_review" value="' . $overall_rating . '" id="customer_review" required />';
    echo '<div id="admin-blik-stars-customerrating" data-rating="' . $overall_rating . '"></div>';
}

function blik_stars_meta_save_ratinginfo($post_id) {
    global $wpdb;
    if (!isset($_POST['starrating_meta_box_nonce'])) {
        return;
    }

    if (!wp_verify_nonce($_POST['starrating_meta_box_nonce'], 'starrating_meta_box')) {
        return;
    }
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {

        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    } else {

        if (!current_user_can('edit_post', $post_id)) {
            return;
        }
    }
    if (!isset($_POST['customer_review'])) {
        return;
    }

    $my_rating = $_POST['customer_review'];

    $user_rating = (empty($my_rating)) ? 0 : $my_rating;
    $current_user = wp_get_current_user();
    $user_ID = $current_user->ID;

    $result = $wpdb->get_row("SELECT * FROM " . BLIKSTARSVOTESTBL . " WHERE post_id=$post_id");
    if ($result) {
        $wpdb->update(BLIKSTARSVOTESTBL, array('overall_rating' => number_format($user_rating, 1)), array('id' => $result->id));
    } else {
        $wpdb->insert(BLIKSTARSVOTESTBL, array(
            'post_id' => $post_id,
            'reviewer_id' => $user_ID,
            'overall_rating' => number_format($user_rating, 1),
            'number_of_votes' => 0,
            'sum_votes' => 0.0,
            'review_type' => 'Other'
                )
        );
    }
}

function blik_stars_add_schema($content) {
    global $wpdb, $post;

    $post_id = get_the_ID();
    $post_type = get_post_type($post_id);

    if ( 'reviews' == $post_type ) {
        $schema = blik_stars_get_rating();
        $content = $content . $schema;
    }

    return $content;
}

function blik_stars_get_rating( $post_id = null ){
    global $wpdb, $post;
    
    if(empty($post_id)){
        $post_id = get_the_ID();
    }
    $blik_stars_votes_table = BLIKSTARSVOTESTBL;
    
    $result = $wpdb->get_row("SELECT * FROM " . $blik_stars_votes_table . " WHERE post_id=$post_id");
    $overall_rating = $result->overall_rating;
    $post_type = get_post_type($post_id);
    
    $schema = "<div class=\"blik-stars-ratings rateit\" id=\"blik_stars_rate_$post_id\"  data-rating=\"$overall_rating\"></div>";
    return $schema;
}

function blik_stars_get_overall_rating() {
    echo blik_stars_get_overall_rating_shortcode();
}

function blik_stars_get_overall_rating_shortcode(){
    global $wpdb;    

    $blik_stars_votes_table = BLIKSTARSVOTESTBL;
    $overall_rating = $wpdb->get_row("SELECT count({$blik_stars_votes_table}.id) as total, ROUND(avg(overall_rating), 1) as average FROM {$blik_stars_votes_table}, {$wpdb->posts} WHERE {$wpdb->posts}.ID={$blik_stars_votes_table}.post_id and {$wpdb->posts}.post_status='publish' ");    
    ob_start();
        ?>
        <div id="blik_stars-aggreegate-rate" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">          
            <div class="blik_stars-ratings" id="blik_stars-ratings-overall" data-rating="<?php echo $overall_rating->average; ?>"></div>
            <div id="blik_stars-ratings-meta">
                <span itemprop="rating" itemscope itemtype="http://data-vocabulary.org/Rating">
                    <span itemprop="average"><?php echo $overall_rating->average; ?></span>
                </span>
                Gebaseerd <span itemprop="votes"><?php echo $overall_rating->total; ?></span> Beoordelingen.
            </div>
        </div>
    <?php  
    $output = ob_get_contents();
    ob_end_clean();
    return $output ;
}

function blik_stars_add_rating() {
    echo blik_stars_add_rating_shortcode();
}

function blik_stars_add_rating_shortcode() {
    global $wpdb;
    $message = '';
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();
        $display_name = $current_user->display_name;
        $user_ID = $current_user->ID;
    } else {
        $user_ID = 1;
        $display_name = '';
    }
    if (isset($_POST['customer_review'])) {
        $reviewed_by = filter_input(INPUT_POST, 'reviewed_by');
        $reviewed_message = filter_input(INPUT_POST, 'review_message');
        $customer_review = filter_input(INPUT_POST, 'customer_review');

        $customer_review_post = array(
            'post_title' => $reviewed_by,
            'post_content' => $reviewed_message,
            'post_status' => 'draft',
            'post_type' => 'reviews',
            'post_author' => $user_ID
        );

        $review_ID = wp_insert_post($customer_review_post);

        if ($review_ID) {
            //update_post_meta($review_ID, '_reviewed_by', $reviewed_by);
            $wpdb->insert(BLIKSTARSVOTESTBL, array(
                'post_id' => $review_ID,
                'reviewer_id' => $user_ID,
                'overall_rating' => number_format($customer_review, 1),
                'number_of_votes' => 0,
                'sum_votes' => 0.0,
                'review_type' => 'Other'
                    )
            );
            $blik_stars_frm_success_message = esc_attr(get_option('blik_stars_frm_success_message', 'Uw beoordeling is succesvol ingediend'));
            $message = "<div class='review-success'> $blik_stars_frm_success_message </div>";
        } else {
            $blik_stars_frm_failure_message = esc_attr(get_option('blik_stars_frm_failure_message', 'Probeer het na een tijdje nog eens. !!!'));
            $message = "<div class='review-error'> $blik_stars_frm_failure_message </div>";
        }
    }
    ob_start();
    ?>
    <div class="blik-stars-add-rate-form" id="blik-stars-add-rate-form-wrapper">            
        <form method="post" enctype="multipart/form-data" id="gtestform_2" action="">
            <div class="review_form_heading">
                <h3 class="review_form_title" style="margin-top: 0;"><?php echo esc_attr(get_option('blik_stars_frm_title', 'Geef hier uw rating')); ?></h3>
                <span class="review_form_description"><?php echo esc_attr(get_option('blik_stars_frm_info', 'Laat hier uw feedback en rating achter.')); ?></span>
            </div>
            <div class="form-group">
                    <label for="ratingInput"><?php echo esc_attr(get_option('blik_stars_frm_label_select_rating', 'Selecteer uw rating')); ?> (<span id="customer_rate">0</span>)</label>
                    <input type="hidden" name="customer_review" value="0" id="customer_review" required />
                    <div id="blik-stars-customerrating"></div>
                </div> 
            <div id="review_form_fields_2" class="review_form_body">
                <?php echo $message; ?>
                <div class="form-group">
                    <label for="reviewed_by"><?php echo esc_attr(get_option('blik_stars_frm_label_name', 'Naam of Bedrijf')); ?></label>
                    <input type="text" name="reviewed_by" class="form-control" id="reviewed_by" required>
                </div>
                <div class="form-group">
                    <label for="review_message"><?php echo esc_attr(get_option('blik_stars_frm_label_review', 'Bericht')); ?></label>
                    <textarea name="review_message" class="form-control" rows="5" id="review_message" required></textarea>
                </div>

            </div>
            <button type="submit" class="btn btn-primary"><?php echo esc_attr(get_option('blik_stars_frm_label_submit', 'Verstuur')); ?></button>
        </form>
    </div>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output ;
}

/**
 * Deprecated Functions.
 */
function blikstars_get_overall_rating() {
    blik_stars_get_overall_rating();
}

function submit_rating() {
    blik_stars_add_rating();
}
