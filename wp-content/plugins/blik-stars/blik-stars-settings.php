<!--- file for the Blik stars plugin settings page --->
<?php

/**
 * add the action to add an extra submenu in the wordpress settings admin page 
 */
add_action('admin_menu', 'blik_stars_add_admin_menu');


/**
 *  Function to add the blik stars settings page in the wordpress admin settings page with the chosen name
 */
function blik_stars_add_admin_menu() {
    add_options_page('Blik Star Rating', 'Blik Star Rating', 'manage_options', 'blik_stars_rating', 'blik_stars_rating_settings_page');
    add_action('admin_init', 'blik_stars_settings_init');
}

/**
 *  Function to register all the custom settings in the settings page.
 */
function blik_stars_settings_init() {
/** Register form for the custom title */
    register_setting('blik-stars-settings', 'blik_stars_frm_title');
/** Register form for the custom title */

    register_setting('blik-stars-settings', 'blik_stars_frm_label_name');
/** Register form for the custom label name */

    register_setting('blik-stars-settings', 'blik_starsfrm_label_review');
/** Register form for the custom label review name */

    register_setting('blik-stars-settings', 'blik_stars_frm_info');
/** Register form for the custom info text */

    register_setting('blik-stars-settings', 'blik_stars_frm_label_select_rating');
/** Register form for the custom select starstext */

    register_setting('blik-stars-settings', 'blik_stars_frm_label_submit');
/** Register form for the custom submit button */

    register_setting('blik-stars-settings', 'blik_stars_frm_success_message');
/** Register form for the custom success message */

    register_setting('blik-stars-settings', 'blik_stars_frm_failure_message');
/** Register form for the custom error message */

}

/**
 *  the blik stars settings php file
 */
function blik_stars_rating_settings_page() {
    ?>
    <div class="wrap">
        <!-- Plugin settings page title -->

        <h2>Blik Stars Rating</h2>

        <!-- information about the Plugin settings page -->

        <p>Instellingen voor Blik stars</p>
        <form method="post" action="options.php">
            <!-- get the  -->

            <?php settings_fields('blik-stars-settings'); ?>
            <?php do_settings_sections('blik-stars-settings'); ?>

            <!-- Blik stars inputfield -->

            <table class="form-table">
                <tr valign="top">
                <!-- Title form text -->

                    <th scope="row">Titel</th>
                    <td>
                        <input type="text" name="blik_stars_frm_title" class="regular-text" value="<?php echo esc_attr(get_option('blik_stars_frm_title', 'Geef hier uw rating')); ?>" />
                    </td>
                </tr>
                <!-- more information text -->

                <tr valign="top">
                    <th scope="row">Infromatie</th>
                    <td>
                        <textarea style="width: 25em;height: 10em;" name="blik_stars_frm_info"><?php echo esc_attr(get_option('blik_stars_frm_info', 'Laat hier uw feedback en rating achter.')); ?></textarea>          
                    </td>
                </tr>
                <!-- Name or business inputfield text -->

                <tr valign="top">
                    <th scope="row">Naam of Bedrijfnaam</th>
                    <td>
                        <input type="text" class="regular-text" name="blik_stars_frm_label_name" value="<?php echo esc_attr(get_option('blik_stars_frm_label_name', 'Naam of Bedrijf')); ?>" />
                    </td>
                </tr>

                 <!-- Review inputfield text -->

                <tr valign="top">
                    <th scope="row">Review</th>
                    <td>
                        <input type="text" class="regular-text" name="blik_stars_frm_label_review" value="<?php echo esc_attr(get_option('blik_stars_frm_label_review', 'Bericht')); ?>" />
                    </td>
                </tr>

                <!-- Star seleter text -->

                <tr valign="top">
                    <th scope="row">Selecteer Ster</th>
                    <td>
                        <input type="text" class="regular-text" name="blik_stars_frm_label_select_rating" value="<?php echo esc_attr(get_option('blik_stars_frm_label_select_rating', 'Selecteer uw rating')); ?>" />
                    </td>
                </tr>
                 <!-- Send review button text -->

                <tr valign="top">
                    <th scope="row">Verstuur Button</th>
                    <td>
                        <input type="text" class="regular-text" name="blik_stars_frm_label_submit" value="<?php echo esc_attr(get_option('blik_stars_frm_label_submit', 'Verstuur')); ?>" />
                    </td>
                </tr>
                
                 <!-- Success message -->

                <tr valign="top">
                    <th scope="row">Success Melding.</th>
                    <td><input type="text" class="regular-text" name="blik_stars_frm_success_message" value="<?php echo esc_attr(get_option('blik_stars_frm_success_message', 'Uw beoordeling is succesvol ingediend')); ?>" /></td>
                </tr>
                 <!-- Error message -->

                <tr valign="top">
                    <th scope="row">Fout Melding.</th>
                    <td><input type="text" class="regular-text" name="blik_stars_frm_failure_message" value="<?php echo esc_attr(get_option('blik_stars_frm_failure_message', 'Probeer het na een tijdje nog eens. !!!')); ?>" /></td>
                </tr>
            </table>

            <!-- save changes button -->
            <?php submit_button(); ?>

        </form>
    </div>
<?php } ?>