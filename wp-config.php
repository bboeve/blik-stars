<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blik_stars');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1q(~{|<-5|!ifEqx(;eG&Y2axrHTUG=+#v?%2hYRRAPjEFXh%A^f3U]6W8}rBpWW');
define('SECURE_AUTH_KEY',  'zK6HM._7HnHA,{0(nwY3jhgO8}QB+$L&<EjdZ(4u${?&,U?^b{6])mP{ig&,gx7P');
define('LOGGED_IN_KEY',    'J3(H#Z>Q~EP_iW@.}v*wD[a$n<sr2u/}E6DxwxX(oqH}P/Lb/[jj&8W@2o36Imz[');
define('NONCE_KEY',        'EQBEw>usx!!h[%LT#3g4)4M!fU;pnTpA{jBqv2kUy#&XW$sDp,LL=v]+8v 2wBZT');
define('AUTH_SALT',        '@GcF )>o?bRTiaHyYzP]{CjCc}20Z9E<1igk;~zuIg]? >[9Vh}cmk2F36pKmjR9');
define('SECURE_AUTH_SALT', 'E@6Al]QD6g`I+mzg#>]gLC!%B&@wn3leCH*)B?e{#CV_$JgXX)%3WS/q5 Y4<[T/');
define('LOGGED_IN_SALT',   'Zj_`PAmxP=-$jtqb4zUg-iZgQx+k-!7+H2Vj}djLq(/E)K&l,Rqeuw]w$?^g=i(D');
define('NONCE_SALT',       ':1scjS;2WEn.LG3^?@x,g<y.C#kF4.SIAiEi*,n<)5-S4$$_+pbve~hxj?1+i`k0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
